package com.myntra.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myntra.sql.connector.RedShiftConnectionFactory;
import com.myntra.vo.ElasticityRackVo;
import com.myntra.vo.ListViews;

public class CompleteClass {


	static String listViewsSql = "select s.style_id, sum(list_count) List, sum(pdp_count) PDP, " +
			" sum(add_to_cart_count) Cart, sum(unit_sold) qtySold " +
			" from fact_basic_funnel_details_snapshot s " +
			" where s.load_date in (%s) " +
			" group by s.style_id ";

	static String skuSql = "select sku_id, style_id, brand, brand_type, article_type, gender, sum(quantity) as quantity " +
			" from bidb.fact_core_item " +
			" where order_created_date in (%s) and (is_shipped=1 or is_realised =1) and store_id = 1" +
			" group by sku_id, style_id, brand, brand_type, article_type, gender ";

	static String brokenSql = "select style_id, sku_id, sum(is_live_on_portal) as isLive from fact_product_snapshot " +
			" where date in (%s) " +
			" group by style_id, sku_id ";


	static Map<String, ListViews> styleInfoMap = new HashMap<String, ListViews>();
	
	
	static long beforePdpTotal = 0;
	static long afterPdpTotal = 0;

	//private static CassandraConnector cassandraConnector = new CassandraConnector();
	final static String[] cassandraIP = {"10.175.146.116","10.162.45.12","10.151.2.83"};

	private static Logger logger = LoggerFactory.getLogger(CompleteClass.class);

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws Exception{

		
	/*	cassandraConnector.connect(cassandraIP, 9042);
		Session session = cassandraConnector.getSession();*/
		
		Connection connection = RedShiftConnectionFactory.getConnection();

		Statement statement = connection.createStatement();
		
		Map<String, ListViews> beforeSkuMap = run(statement, "20160427,20160428,20160429,20160502,20160503,20160504,20160505", "before");
		
		Map<String, ListViews> afterSkuMap = run(statement, "20160510,20160511,20160512,20160513,20160516,20160517,20160518", "after");
		
		//Map<String, ElasticityRackVo> rackMap = RackReader.readRackInfo(session);
		
		//analyse(beforeSkuMap, afterSkuMap, rackMap);
		analyse(beforeSkuMap, afterSkuMap);
		
		
		
		statement.close();
		connection.close();

		if(statement != null) {
			statement.close();
			logger.info("Closing statement object");
		}
		if(connection != null) {
			connection.close();
			logger.info("Closing Connection object");
		}
		
		
		
		
		
		/*try {
			CompleteClass mainClass = new CompleteClass();
			mainClass.run(args);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Process completed");
		}*/
		System.exit(0);
	}

	
	public static Map<String, ListViews> run(Statement statement, String formattedDate, String timeStatus) throws Exception{
		// TODO Auto-generated method stub

		Map<String, ListViews> beforeSkuMap = null;

		if (statement != null) {

			ResultSet rs1 = statement.executeQuery(String.format(listViewsSql, formattedDate));
			Map<String, ListViews> beforeMap = trackListViews(rs1, formattedDate, timeStatus);

			ResultSet rs2 = statement.executeQuery(String.format(skuSql, formattedDate));
			beforeSkuMap = trackskuData(rs2, formattedDate);

			ResultSet rs3 = statement.executeQuery(String.format(brokenSql, formattedDate));
			Map<String, ListViews> beforebrokenMap = trackbrokenData(rs3, formattedDate);


			for(Map.Entry<String, ListViews> entry : beforeSkuMap.entrySet()) {
				ListViews vo = entry.getValue();
				String styleId = vo.getStyle_id();
				String skuId = vo.getSku_id();

				ListViews styleDetails = beforeMap.get(styleId);
				if (styleDetails != null) {
					vo.setList(styleDetails.getList());
					vo.setPdp(styleDetails.getPdp());
					vo.setCart(styleDetails.getCart());
					vo.setStyleQtySold(styleDetails.getStyleQtySold());
				}

				ListViews skuDetails = beforebrokenMap.get(skuId);
				if (skuDetails != null) {
					vo.setIslive(skuDetails.getIslive());
				}

				beforeSkuMap.put(skuId, vo);
				styleInfoMap.put(styleId, vo);
			}
		}
		return beforeSkuMap;
	}

	private static Map<String, ListViews> trackbrokenData(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setSku_id(rs.getString("sku_id"));
			listViews.setIslive(rs.getInt("isLive"));
			styleMap.put(listViews.getSku_id(), listViews);
		}
		return styleMap;
	}

	private static Map<String, ListViews> trackskuData(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setSku_id(rs.getString("sku_id"));
			listViews.setArticle_type(rs.getString("article_type"));
			listViews.setBrand_type(rs.getString("brand_type"));
			listViews.setBrand(rs.getString("brand"));
			listViews.setGender(rs.getString("gender"));
			listViews.setSkuQtySold(rs.getLong("quantity"));
			styleMap.put(listViews.getSku_id(), listViews);
		}
		return styleMap;
	}

	private static Map<String, ListViews> trackListViews(ResultSet rs, String formattedDate, String timeStatus) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setList(rs.getLong("list"));
			listViews.setPdp(rs.getLong("pdp"));
			
			
			if (timeStatus.equals("before")) {
				beforePdpTotal += rs.getLong("pdp");
			} else if (timeStatus.equals("after")) {
				afterPdpTotal += rs.getLong("pdp");
			}
			
			listViews.setCart(rs.getLong("cart"));
			listViews.setStyleQtySold(rs.getLong("qtySold"));
			styleMap.put(listViews.getStyle_id(), listViews);
		}
		return styleMap;
	}
	
	
	
	private static void analyse(Map<String, ListViews> beforeSkuMap2, Map<String, ListViews> afterSkuMap2, Map<String, ElasticityRackVo> rackMap) {
		
		Map<String, Map<String,ListViews>> beforeMap = new HashMap<String, Map<String,ListViews>>();
		
		for (Map.Entry<String, ListViews> entry : beforeSkuMap2.entrySet()) {
			ListViews vo = entry.getValue();
			if (beforeMap.containsKey(vo.getStyle_id())) {
				Map<String,ListViews> map = beforeMap.get(vo.getStyle_id());
				map.put(vo.getSku_id(), vo);
				beforeMap.put(vo.getStyle_id(), map);
			} else {
				Map<String,ListViews> map = new HashMap<String, ListViews>();
				map.put(vo.getSku_id(), vo);
				beforeMap.put(vo.getStyle_id(), map);
			}
		}
		
		Map<String, Map<String,ListViews>> afterMap = new HashMap<String, Map<String,ListViews>>();
		
		for (Map.Entry<String, ListViews> entry : afterSkuMap2.entrySet()) {
			ListViews vo = entry.getValue();
			if (afterMap.containsKey(vo.getStyle_id())) {
				Map<String,ListViews> map = afterMap.get(vo.getStyle_id());
				map.put(vo.getSku_id(), vo);
				afterMap.put(vo.getStyle_id(), map);
			} else {
				Map<String,ListViews> map = new HashMap<String, ListViews>();
				map.put(vo.getSku_id(), vo);
				afterMap.put(vo.getStyle_id(), map);
			}
		}
		
	    
	    for (Map.Entry<String, Map<String,ListViews>> entry : afterMap.entrySet()) {
	    	
	    	String style = entry.getKey();
	    	Map<String, ListViews> afterSkuMap = entry.getValue();
	    	Map<String, ListViews> beforeSkuMap = beforeMap.get(style);
	    	
	    	int totalQtySoldBefore = 0;
	    	int totalQtySoldAfter = 0;
	    	
	    	long beforePdpCount = 0;
	    	long afterPdpCount = 0;
	    	
	    	int beforeIsLiveMax = 0;
	    	int afterIsLiveMax = 0;
	    	
	    	if (beforeSkuMap!=null) {

	    		Set<String> skuSet = new HashSet<String>();
	    		
		    	for (Map.Entry<String, ListViews> skuEntry : beforeSkuMap.entrySet()) {
		    		ListViews val = skuEntry.getValue();
		    		beforePdpCount = val.getPdp();
		    		totalQtySoldBefore += val.getSkuQtySold();
		    		if (val.getIslive() > beforeIsLiveMax) {
		    			beforeIsLiveMax = val.getIslive();
		    		}
		    		
		    		skuSet.add(skuEntry.getKey());
		    	}
		    	
		    	for (Map.Entry<String, ListViews> skuEntry : afterSkuMap.entrySet()) {
		    		ListViews val = skuEntry.getValue();
		    		afterPdpCount = val.getPdp();
		    		totalQtySoldAfter += val.getSkuQtySold();
		    		if (val.getIslive() > afterIsLiveMax) {
		    			afterIsLiveMax = val.getIslive();
		    		}
		    		
		    		skuSet.add(skuEntry.getKey());
		    	}
		    	
		    	//System.out.println("  ********************************************  ");
		    	
		    	//System.out.println(style + "  " + totalQtySoldBefore + "  " + totalQtySoldAfter);
		    	
		    	
		    	int beforeSkuCount = 0;
		    	double beforeStyleVal = 0;
		    	
		    	int afterSkuCount = 0;
		    	double afterStyleVal = 0;
		    	
		    	for (String sku : skuSet) {
		    		
		    		ListViews afterSkuVO = afterSkuMap.get(sku);
		    		ListViews beforeSkuVO = beforeSkuMap.get(sku);
		    		
		    		if (beforeSkuVO != null && afterSkuVO != null) {
		    			if (beforeSkuVO.getIslive() != 0) {
		    				beforeSkuCount++;
		    				beforeStyleVal += ((double)beforeSkuVO.getSkuQtySold()/(double)beforeSkuVO.getIslive());
		    			}
		    			if (afterSkuVO.getIslive() != 0) {
		    				afterSkuCount++;
		    				afterStyleVal += ((double)afterSkuVO.getSkuQtySold()/(double)afterSkuVO.getIslive());
		    			}
		    			System.out.println(sku + " #### " + beforeSkuVO.getIslive() + " *** " + beforeSkuVO.getSkuQtySold() + " ######## " + afterSkuVO.getIslive() + " *** " + afterSkuVO.getSkuQtySold() );
		    		} else if (beforeSkuVO != null) {
		    			if (beforeSkuVO.getIslive() != 0) {
		    				beforeSkuCount++;
		    				beforeStyleVal += ((double)beforeSkuVO.getSkuQtySold()/(double)beforeSkuVO.getIslive());
		    			}
		    			System.out.println(sku + " #### " + beforeSkuVO.getIslive() + " *** " + beforeSkuVO.getSkuQtySold() + " ######## " + afterSkuVO + " *** " + afterSkuVO );
		    			//System.out.println(sku + " *** " + beforeSkuVO.getSkuQtySold() + " *** " + afterSkuVO );
		    		} else if (afterSkuVO != null) {
		    			if (afterSkuVO.getIslive() != 0) {
		    				afterSkuCount++;
		    				afterStyleVal += ((double)afterSkuVO.getSkuQtySold()/(double)afterSkuVO.getIslive());
		    			}
		    			System.out.println(sku + " #### " + beforeSkuVO + " *** " + beforeSkuVO + " ######## " + afterSkuVO.getIslive() + " *** " + afterSkuVO.getSkuQtySold() );
		    			//System.out.println(sku + " *** " + beforeSkuVO + " *** " + afterSkuVO.getSkuQtySold() );
		    		}
		    	}
		    	
		    	//System.out.println("Style Val : before : " + (beforeStyleVal/beforeSkuCount)/beforePdpCount + " after : " + (afterStyleVal/afterSkuCount)/afterPdpCount);
		    	System.out.println(" *** " + beforeStyleVal +" *** "+beforePdpCount+" *** "+beforeIsLiveMax+" *** "+afterStyleVal+" *** "+afterPdpCount+" *** "+afterIsLiveMax);
		    	System.out.println("Style Val : before : " + (beforeStyleVal/(beforePdpCount/beforeIsLiveMax)) + " after : " + (afterStyleVal/(afterPdpCount/afterIsLiveMax)));
	    	}
	    	
	    }
	    
	    
	    
	}

	

	private static void analyse(Map<String, ListViews> beforeSkuMap2, Map<String, ListViews> afterSkuMap2) {
		
		//style level sku data map
		Map<String, Map<String,ListViews>> beforeMap = new HashMap<String, Map<String,ListViews>>();
		
		
		for (Map.Entry<String, ListViews> entry : beforeSkuMap2.entrySet()) {
			ListViews vo = entry.getValue();
			if (beforeMap.containsKey(vo.getStyle_id())) {
				Map<String,ListViews> map = beforeMap.get(vo.getStyle_id());
				map.put(vo.getSku_id(), vo);
				beforeMap.put(vo.getStyle_id(), map);
			} else {
				Map<String,ListViews> map = new HashMap<String, ListViews>();
				map.put(vo.getSku_id(), vo);
				beforeMap.put(vo.getStyle_id(), map);
			}
		}
		
		//style level sku data map
		Map<String, Map<String,ListViews>> afterMap = new HashMap<String, Map<String,ListViews>>();
		
		for (Map.Entry<String, ListViews> entry : afterSkuMap2.entrySet()) {
			ListViews vo = entry.getValue();
			if (afterMap.containsKey(vo.getStyle_id())) {
				Map<String,ListViews> map = afterMap.get(vo.getStyle_id());
				map.put(vo.getSku_id(), vo);
				afterMap.put(vo.getStyle_id(), map);
			} else {
				Map<String,ListViews> map = new HashMap<String, ListViews>();
				map.put(vo.getSku_id(), vo);
				afterMap.put(vo.getStyle_id(), map);
			}
		}
		
		
		int equalCount = 0;
	    int positiveCount = 0;
	    int negativeCount = 0;
	    
	    List<String> equalList = new ArrayList<String>();
	    List<String> positiveList = new ArrayList<String>();
	    List<String> negativeList = new ArrayList<String>();
	    
	    int pdpIncQtyInc = 0;
	    int pdpDecQtyInc = 0;
	    int pdpIncQtyDec = 0;
	    int pdpDecQtyDec = 0;
	    
	    /*String pdpIncQtyIncStr = "";
	    String pdpDecQtyIncStr = "";
	    String pdpIncQtyDecStr = "";
	    String pdpDecQtyDecStr = "";*/
	    
	    List<String> pdpIncQtyIncList = new ArrayList<String>();
	    List<String> pdpDecQtyIncList = new ArrayList<String>();
	    List<String> pdpIncQtyDecList = new ArrayList<String>();
	    List<String> pdpDecQtyDecList = new ArrayList<String>();
	    
	    for (Map.Entry<String, Map<String,ListViews>> entry : afterMap.entrySet()) {
	    	
	    	String style = entry.getKey();
	    	
	    	//if (style.equals("1311404")) {

    		
    		Map<String, ListViews> afterSkuMap = entry.getValue();
	    	Map<String, ListViews> beforeSkuMap = beforeMap.get(style);
	    	
	    	//if (afterSkuMap != null && beforeSkuMap != null && afterSkuMap.size() < beforeSkuMap.size()) {
	    	
	    	System.out.println("**********************************************");
	    	
	    	int totalQtySoldBefore = 0;
	    	int totalQtySoldAfter = 0;
	    	
	    	long beforePdpCount = 0;
	    	long afterPdpCount = 0;
	    	
	    	int beforeIsLiveMax = 0;
	    	int afterIsLiveMax = 0;
	    	
	    	if (beforeSkuMap!=null) {

	    		Set<String> skuSet = new HashSet<String>();
	    		
		    	for (Map.Entry<String, ListViews> skuEntry : beforeSkuMap.entrySet()) {
		    		ListViews val = skuEntry.getValue();
		    		beforePdpCount = val.getPdp();
		    		totalQtySoldBefore += val.getSkuQtySold();
		    		if (val.getIslive() > beforeIsLiveMax) {
		    			beforeIsLiveMax = val.getIslive();
		    		}
		    		
		    		skuSet.add(skuEntry.getKey());
		    	}
		    	
		    	for (Map.Entry<String, ListViews> skuEntry : afterSkuMap.entrySet()) {
		    		ListViews val = skuEntry.getValue();
		    		afterPdpCount = val.getPdp();
		    		totalQtySoldAfter += val.getSkuQtySold();
		    		if (val.getIslive() > afterIsLiveMax) {
		    			afterIsLiveMax = val.getIslive();
		    		}
		    		
		    		skuSet.add(skuEntry.getKey());
		    	}
		    	
		    	//System.out.println("  ********************************************  ");
		    	
		    	//System.out.println(style + "  " + totalQtySoldBefore + "  " + totalQtySoldAfter);
		    	
		    	
		    	int beforeSkuCount = 0;
		    	double beforeStyleVal = 0;
		    	
		    	int afterSkuCount = 0;
		    	double afterStyleVal = 0;
		    	
		    	for (String sku : skuSet) {
		    		
		    		ListViews afterSkuVO = afterSkuMap.get(sku);
		    		ListViews beforeSkuVO = beforeSkuMap.get(sku);
		    		
		    		if (beforeSkuVO != null && afterSkuVO != null) {
		    			if (beforeSkuVO.getIslive() != 0) {
		    				beforeSkuCount++;
		    				beforeStyleVal += ((double)beforeSkuVO.getSkuQtySold()/(double)beforeSkuVO.getIslive());
		    			}
		    			if (afterSkuVO.getIslive() != 0) {
		    				afterSkuCount++;
		    				afterStyleVal += ((double)afterSkuVO.getSkuQtySold()/(double)afterSkuVO.getIslive());
		    			}
		    			System.out.println(sku + " #### " + beforeSkuVO.getIslive() + " *** " + beforeSkuVO.getSkuQtySold() + " ######## " + afterSkuVO.getIslive() + " *** " + afterSkuVO.getSkuQtySold() );
		    		} else if (beforeSkuVO != null) {
		    			if (beforeSkuVO.getIslive() != 0) {
		    				beforeSkuCount++;
		    				beforeStyleVal += ((double)beforeSkuVO.getSkuQtySold()/(double)beforeSkuVO.getIslive());
		    			}
		    			System.out.println(sku + " #### " + beforeSkuVO.getIslive() + " *** " + beforeSkuVO.getSkuQtySold() + " ######## " + afterSkuVO + " *** " + afterSkuVO );
		    			//System.out.println(sku + " *** " + beforeSkuVO.getSkuQtySold() + " *** " + afterSkuVO );
		    		} else if (afterSkuVO != null) {
		    			if (afterSkuVO.getIslive() != 0) {
		    				afterSkuCount++;
		    				afterStyleVal += ((double)afterSkuVO.getSkuQtySold()/(double)afterSkuVO.getIslive());
		    			}
		    			System.out.println(sku + " #### " + beforeSkuVO + " *** " + beforeSkuVO + " ######## " + afterSkuVO.getIslive() + " *** " + afterSkuVO.getSkuQtySold() );
		    			//System.out.println(sku + " *** " + beforeSkuVO + " *** " + afterSkuVO.getSkuQtySold() );
		    		}
		    	}
		    	
		    	//System.out.println("Style Val : before : " + (beforeStyleVal/beforeSkuCount)/beforePdpCount + " after : " + (afterStyleVal/afterSkuCount)/afterPdpCount);
		    	if (beforeIsLiveMax != 0 && afterIsLiveMax != 0) {
		    		//System.out.println(" *** " + beforeStyleVal +" *** "+beforePdpCount+" *** "+beforeIsLiveMax+" *** "+afterStyleVal+" *** "+afterPdpCount+" *** "+afterIsLiveMax);
		    		
		    	
		    		//for normalized conversion
			    	double beforeConversion = ((beforeStyleVal*(skuSet.size()/beforeSkuCount))/(beforePdpCount/beforeIsLiveMax));
			    	double afterConversion = ((afterStyleVal*(skuSet.size()/afterSkuCount))/(afterPdpCount/afterIsLiveMax));
			    	
			    	
			    	//for actual conversion
			    	/*double beforeConversion = ((beforeStyleVal)/(beforePdpCount/beforeIsLiveMax));
			    	double afterConversion = ((afterStyleVal)/(afterPdpCount/afterIsLiveMax));
			    	*/

			    	double convChange = (100/beforeConversion)*afterConversion;
			    	
			    	String ConvVal = "";
			    	if (convChange < 120 && convChange > 80) {
			    		ConvVal = "equal";
			    		equalCount++;
			    		equalList.add(style);
			    	} else if (convChange > 120) {
			    		ConvVal = "positive";
			    		positiveCount++;
			    		positiveList.add(style);
			    	} else if (convChange < 80) {
			    		ConvVal = "negative";
			    		negativeCount++;
			    		negativeList.add(style);
			    	}
			    	
			    	
			    	//System.out.println(style+" ### "+beforePdpCount+" *** "+afterPdpCount+" ### "+totalQtySoldBefore+" *** "+totalQtySoldAfter +" ### "+convDiff+" *** "+ConvVal);
			    	
			    	//System.out.println(style+" ### "+beforePdpTotal+" *** "+afterPdpTotal+" ### "+beforePdpCount+" *** "+afterPdpCount+" ### "+totalQtySoldBefore+" *** "+totalQtySoldAfter +" ### "+convDiff+" *** "+ConvVal);
			    	
			    	System.out.println(style+" ### "+convChange+" ### "+beforeConversion+" *** "+afterConversion+" ### "+beforePdpTotal+" *** "+afterPdpTotal+" ### "+beforePdpCount+" *** "+afterPdpCount+" ### "+totalQtySoldBefore+" *** "+totalQtySoldAfter +" *** "+ConvVal);
			    	
			    	
			    	double beforePdpContribution = (double)beforePdpCount/((double)beforePdpTotal/10000);
			    	double afterPdpContribution = (double)afterPdpCount/((double)afterPdpTotal/10000);
			    	
			    	double pdpContributionChange = (100/beforePdpContribution)*afterPdpContribution;
			    	
			    	//double pdpConversion = ((double)afterPdpCount/((double)afterPdpTotal/10000))-((double)beforePdpCount/((double)beforePdpTotal/10000));
			    	
			    	if (pdpContributionChange > 120 && ConvVal.equals("positive")) {
			    		pdpIncQtyInc++;
			    		pdpIncQtyIncList.add(style);
			    	}
			    	if (pdpContributionChange > 120 && ConvVal.equals("negative")) {
			    		pdpIncQtyDec++;
			    		pdpIncQtyDecList.add(style);
			    	}
			    	if (pdpContributionChange < 80 && ConvVal.equals("positive")) {
			    		pdpDecQtyInc++;
			    		pdpDecQtyIncList.add(style);
			    	}
			    	if (pdpContributionChange < 80 && ConvVal.equals("negative")) {
			    		pdpDecQtyDec++;
			    		pdpDecQtyDecList.add(style);
			    	}
		    	
		    	}
		    	////
	    	}
	    	
	     //} //if(style)  or //if (afterSkuMap.size() < beforeSkuMap.size()) {
	    	
	    }
	    
	    System.out.println("**********************************************");
	    
	    System.out.println("Equal : " + equalCount +"  positive : "+ positiveCount + " negative : " + negativeCount);
	    System.out.println(pdpIncQtyInc + "    " + pdpIncQtyDec + "    " + pdpDecQtyInc + "   " + pdpDecQtyDec);
	    
	    
	    System.out.println("pdpIncQtyIncList  :  "+ pdpIncQtyIncList);
	    System.out.println("pdpDecQtyIncList  :  "+ pdpDecQtyIncList);
	    System.out.println("pdpIncQtyDecList  :  "+ pdpIncQtyDecList);
	    System.out.println("pdpDecQtyDecList  :  "+ pdpDecQtyDecList);
	
	
	   /* System.out.println("equalList : " + equalList);
	    System.out.println("positiveList : " + positiveList);
	    System.out.println("negativeList : " + negativeList);
	
	    System.out.println("#################################################");
	    
	    for (String sty : positiveList) {
	    	if (!pdpIncQtyIncStr.contains(sty) && !pdpDecQtyIncStr.contains(sty)) {
	    		System.out.println(sty);
	    	}
	    }*/
	    
	   /* cassandraConnector.connect(cassandraIP, 9042);
		Session session = cassandraConnector.getSession();
				
		Map<String, ElasticityRackVo> rackMap = RackReader.readRackInfo(session);

		//ComputedSnapshotReader.readComputedSnapshot(session, currentDate, currentTime)
		
	    categoryLevelAnalysis(pdpIncQtyIncList, rackMap, "pdpIncQtyIncList");
	    categoryLevelAnalysis(pdpDecQtyIncList, rackMap, "pdpDecQtyIncList");
	    categoryLevelAnalysis(pdpIncQtyDecList, rackMap, "pdpIncQtyDecList");
	    categoryLevelAnalysis(pdpDecQtyDecList, rackMap, "pdpDecQtyDecList");*/
	
	}


	private static void categoryLevelAnalysis(List<String> list, Map<String, ElasticityRackVo> rackMap, String fileName) {
		
		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			file = new File("./"+fileName+".csv");
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write("style,articleType,gender,elasticity,rack,collection,cascElasticity_CD,cascElasticity_TD,cascElasticity_unif,cascVisibility_Elasticity");
			for (String style : list) {
				bufferedWriter.write("\n");
				ListViews styleInfoVO = styleInfoMap.get(style);
				String brand = styleInfoVO.getBrand();
				String articleType = styleInfoVO.getArticle_type();
				String gender = styleInfoVO.getGender();
				
				ElasticityRackVo vo = rackMap.get(style);
				
				if (vo != null) {
					//bufferedWriter.write(style+","+vo.getArticleType()+","+vo.getGender()+","+vo.getElasticity()+","+vo.getRack()+","+vo.getCollection()+","+vo.getCascElasticity_CD()+","+vo.getCascElasticity_TD()+","+vo.getCascElasticity_unif()+","+vo.getCascVisibility_Elasticity());
					bufferedWriter.write(style+","+articleType+","+gender+","+brand+","+vo.getElasticity()+","+vo.getRack()+","+vo.getCollection()+","+vo.getCascElasticity_CD()+","+vo.getCascElasticity_TD()+","+vo.getCascElasticity_unif()+","+vo.getCascVisibility_Elasticity());
				} else {
					//bufferedWriter.write(style+","+vo);
					bufferedWriter.write(style+","+articleType+","+gender+","+brand+","+vo);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 
		finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	}
	
	
	
	
	
	
	
}
