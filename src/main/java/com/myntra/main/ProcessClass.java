package com.myntra.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.myntra.vo.ExcelVO;
import com.myntra.vo.ListViews;


public class ProcessClass {
	
	public static void main(String[] args) {
		Map<String, ExcelVO> map = readFile("/home/en-sorabhk/files/unbroken_full.csv");
		
		List<ExcelVO> improvedList = new ArrayList<ExcelVO>();
		List<ExcelVO> droppedList = new ArrayList<ExcelVO>();
		List<ExcelVO> equalList = new ArrayList<ExcelVO>();
		
		int i = 0;
		int j = 0;
		int count = 0;
		for(Map.Entry<String, ExcelVO> entry : map.entrySet()) {
			ExcelVO vo = entry.getValue();
			if (vo.getAfterQtySold() != 0 && vo.getBeforeQtySold() != 0) {
				if ((vo.getBeforeConversionQtySoldPdp() - (vo.getAfterConversionQtySoldPdp())) > .05) {
					i++;
					droppedList.add(vo);
					System.out.println(entry.getKey() + "  ****   " + (vo.getBeforeConversionQtySoldPdp() - (vo.getAfterConversionQtySoldPdp()*10/4)));
				} else if ((vo.getBeforeConversionQtySoldPdp() - (vo.getAfterConversionQtySoldPdp())) < -.05){
					j++;
					improvedList.add(vo);
					System.out.println(entry.getKey() + "  ####   " + (vo.getBeforeConversionQtySoldPdp() - (vo.getAfterConversionQtySoldPdp()*10/4)));
				} else {
					equalList.add(vo);
				}
			}
		}
		
		writeToFile(droppedList, improvedList, equalList);
		
		//System.out.println(i + "      " +  count);
		System.out.println(map.size() + "   ***   " + i + "   ***   " + j);
	}	
	
	private static Map<String, ExcelVO> readFile(String fileName) {

		Map<String, ExcelVO> map = new HashMap<String, ExcelVO>();
		
		File file = new File(fileName);
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String str;
	    try {
			String[] strArr = null;
			while ((str = in.readLine()) != null) {
				strArr = str.split(",");
				if (!strArr[0].equalsIgnoreCase("style")) {
				    ExcelVO vo = new ExcelVO();
				    vo.setStyle(strArr[0]);
				    vo.setBeforeList(Integer.parseInt(strArr[1]));
				    vo.setAfterList(Integer.parseInt(strArr[2]));
				    vo.setBeforePdp(Integer.parseInt(strArr[3]));
				    vo.setAfterPdp(Integer.parseInt(strArr[4]));
				    vo.setBeforeCart(Integer.parseInt(strArr[5]));
				    vo.setAfterCart(Integer.parseInt(strArr[6]));
				    vo.setBeforeQtySold(Integer.parseInt(strArr[7]));
				    vo.setAfterQtySold(Integer.parseInt(strArr[8]));
				    vo.setBeforeConversionPdpList(Double.parseDouble(strArr[9]));
				    vo.setAfterConversionPdpList(Double.parseDouble(strArr[10]));
				    vo.setConversionDiffPdpList(Double.parseDouble(strArr[11]));
				    vo.setBeforeConversionCartPdp(Double.parseDouble(strArr[12]));
				    vo.setAfterConversionCartPdp(Double.parseDouble(strArr[13]));
				    vo.setConversionDiffCartPdp(Double.parseDouble(strArr[14]));
				    vo.setBeforeConversionQtySoldPdp(Double.parseDouble(strArr[15]));
				    vo.setAfterConversionQtySoldPdp(Double.parseDouble(strArr[16]));
				    vo.setConversionDiffQtySoldPdp(Double.parseDouble(strArr[17]));
				    map.put(vo.getStyle(), vo);
			    }
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    return map;
	}
	
	
	private static void writeToFile(List<ExcelVO> droppedList, List<ExcelVO> improvedList, List<ExcelVO> equalList) {

		String header = "style,beforeList,afterList,beforePdp,afterPdp,beforeCart,afterCart,beforeQtySold,afterQtySold" +
				",conversionDiff(qtySold/pdp)";
				//",beforeConversion(pdp/list),afterConversion(pdp/list),conversionDiff(pdp/list)" +
				//",beforeConversion(cart/pdp),afterConversion(cart/pdp),conversionDiff(cart/pdp)" +
				//",beforeConversion(qtySold/pdp),afterConversion(qtySold/pdp),conversionDiff(qtySold/pdp)";

		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			file = new File("/home/en-sorabhk/files/unbroken_full_anaysis.csv");
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(header);
			bufferedWriter.write("\n");
			bufferedWriter.write("Sale drop");
			for (ExcelVO vo : droppedList) {
				bufferedWriter.write("\n");
				bufferedWriter.write(vo.getStyle() + "," + vo.getBeforeList() + "," + vo.getAfterList() + 
						"," + vo.getBeforePdp() + "," + vo.getAfterPdp() + 
						"," + vo.getBeforeCart() + "," + vo.getAfterCart() + 
						"," + vo.getBeforeQtySold() + "," + vo.getAfterQtySold() +

						"," + (vo.getBeforeConversionQtySoldPdp() - (vo.getAfterConversionQtySoldPdp()))
						);
			}
			
			bufferedWriter.write("\n");
			bufferedWriter.write("Sale improved");
			for (ExcelVO vo : improvedList) {
				bufferedWriter.write("\n");
				bufferedWriter.write(vo.getStyle() + "," + vo.getBeforeList() + "," + vo.getAfterList() + 
						"," + vo.getBeforePdp() + "," + vo.getAfterPdp() + 
						"," + vo.getBeforeCart() + "," + vo.getAfterCart() + 
						"," + vo.getBeforeQtySold() + "," + vo.getAfterQtySold() +

						"," + (vo.getBeforeConversionQtySoldPdp() - (vo.getAfterConversionQtySoldPdp()))
						);
			}
			
			bufferedWriter.write("\n");
			bufferedWriter.write("Sale Equal");
			for (ExcelVO vo : equalList) {
				bufferedWriter.write("\n");
				bufferedWriter.write(vo.getStyle() + "," + vo.getBeforeList() + "," + vo.getAfterList() + 
						"," + vo.getBeforePdp() + "," + vo.getAfterPdp() + 
						"," + vo.getBeforeCart() + "," + vo.getAfterCart() + 
						"," + vo.getBeforeQtySold() + "," + vo.getAfterQtySold() +

						"," + (vo.getBeforeConversionQtySoldPdp() - (vo.getAfterConversionQtySoldPdp()))
						);
			}
			
		} catch (IOException e) {
			System.out.println("Exception raised while writing to report : " + e);
			e.printStackTrace();
			//logger.error("Exception raised while writing to report : " + e);
		} 
		finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception raised while closing writer objects : "+ e);
				//logger.error("Exception raised while closing writer objects : "+ e);
				e.printStackTrace();
			} 
		}

	}
}
