package com.myntra.main;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Session;
import com.myntra.sql.connector.CassandraConnector;
import com.myntra.sql.connector.RedShiftConnectionFactory;
import com.myntra.vo.CreateDropdownJSON;
import com.myntra.vo.ListViews;
import com.myntra.vo.RevTemp;
import com.myntra.vo.Revenue;

public class MainClassOld {
	
	
	// 3 + brandtype + date range
	static String sql = "select a.*, (a.revenue - a.tax - a.cost + a.Vendor_funding +extra_charges) as RGM " +
						" from (select fo.order_created_date, dp.style_id, dp.commercial_type, dp.brand_type, dp.brand, dp.article_type, " +
						" dp.gender, sum(item_revenue_inc_cashback) as revenue, sum(quantity) as qtySold, " +
						" sum(article_mrp*quantity) as Article_MRP, Sum(fo.product_discount) as TD, " +
						" sum(vendor_funding) as Vendor_funding, sum(fo.Coupon_discount) as CD, " +
						" (max(o.shipping_charges)+max(o.emi_charges)+max(o.gift_charges)) as extra_charges, " + 
						" sum(fo.tax) as tax, sum(item_purchase_price_inc_tax) as cost from " +
						" fact_orderitem fo, dim_product dp, fact_order o " +
						" where fo.idproduct = dp.id and fo.order_id=o.order_id and " +
						" ( fo.order_created_date = %s ) and ( fo.is_realised =1 or fo.is_shipped = 1 ) " +
						" group by fo.order_created_date, dp.style_id, dp.commercial_type, dp.brand_type, dp.brand, dp.article_type, dp.gender ) as a ";
	
	static String listViewsSql = "select load_date, dp.style_id, dp.commercial_type, dp.article_type, dp.brand_type, dp.brand, dp.gender, sum(list_count) List, sum(pdp_count) PDP, sum(add_to_cart_count) Cart " +
									" from " +
									" ( select style_id, commercial_type, article_type, brand_type, brand, gender " +
									" FROM dim_product " +
									" group by style_id, commercial_type, article_type, brand_type, brand, gender " +
									" ) dp, fact_basic_funnel_details_snapshot s " +
									" where s.load_date = %s and dp.style_id = s.style_id " +
									" group by s.load_date, dp.style_id, commercial_type, article_type, dp.brand_type, dp.brand, gender ";
	
	static String insertSql = "insert into dailyrevmetric.dailymetricbrandtracking (date, revTot, tdTot, cdTot, rgmTot, " +
			" commercial_type, article_type, brand_type, brand, gender, rev, td, cd, rgm, rev_cPerc, td_cPerc, " +
			" cd_cPerc, rgm_cPerc, qtysold) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	
	static String insertListViews = "insert into dailyrevmetric.listViewsbrandtracking (date, commercial_type, article_type, brand_type, brand, gender, " +
			" list, pdp, cart ) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	CassandraConnector cassandraConnector = new CassandraConnector();
	final String[] cassandraIP = {"127.0.0.1"};
	//final String[] cassandraIP = {"10.175.146.116"};
	
	private static Logger logger = LoggerFactory.getLogger(MainClassOld.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {

			logger.info("Process started");
			System.out.println("Process started");
			try {
				MainClassOld mainClass = new MainClassOld();
				mainClass.run(args);
			} catch (Exception e) {
				logger.error("Exception raised in run method ", e);
				System.out.println("Exception raised in run method " + e);
				e.printStackTrace();
			} finally {
				logger.info("calling System.exit(0) to kill the process");
				System.out.println("Process completed");
			}
			System.exit(0);
	}

	public void run(String[] args) throws Exception{
		// TODO Auto-generated method stub

		Connection connection = RedShiftConnectionFactory.getConnection();
		
		Statement statement = connection.createStatement();
		
		
		String formattedDate = null;
		if (args != null && args.length>0) {
			formattedDate = args[0];
			try {
				new SimpleDateFormat("yyyyMMdd").parse(formattedDate);
				} catch (Exception ex) {
				// do something for invalid dateformat
					System.out.println("Please enter date in yyyyMMdd format");
					System.exit(0);
				}
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, -1);
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			formattedDate = format.format(calendar.getTime());  
		}
		
		
		//formattedDate = "20151128";
		
		if (statement != null) {
			logger.info("Query processing started at :" + new Date());
			System.out.println("Query processing started at :" + new Date());
			//System.out.println(String.format(sql, formattedDate));
			ResultSet rs = statement.executeQuery(String.format(sql, formattedDate));
			trackContribMetric(rs, formattedDate);
			
			ResultSet rs1 = statement.executeQuery(String.format(listViewsSql, formattedDate));
			trackListViews(rs1, formattedDate);
			
			rs.close();
		}

         statement.close();
         connection.close();
		
		if(statement != null) {
			statement.close();
			logger.info("Closing statement object");
		}
		if(connection != null) {
			connection.close();
			logger.info("Closing Connection object");
		}
	}

	
	private void trackListViews(ResultSet rs, String formattedDate) throws Exception {

		logger.info("Starting tracking list views at : " + new Date());
		System.out.println("Starting tracking list views at : " + new Date());

		cassandraConnector.connect(cassandraIP, 9042);
		Session session = cassandraConnector.getSession();
		
		
		com.datastax.driver.core.PreparedStatement preparedStatement = session.prepare(insertListViews);
		
		BoundStatement boundStatement = new BoundStatement(preparedStatement);

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();
			int formattedDateInt = Integer.parseInt(formattedDate);
			listViews.setDate(formattedDateInt);
			listViews.setCommercial_type(rs.getString("commercial_type"));
			listViews.setArticle_type(rs.getString("article_type"));
			listViews.setBrand_type(rs.getString("brand_type"));
			listViews.setBrand(rs.getString("brand"));
			listViews.setGender(rs.getString("gender"));
			listViews.setList(rs.getLong("list"));
			listViews.setPdp(rs.getLong("pdp"));
			listViews.setCart(rs.getLong("cart"));
			
			/*static String insertListViews = "insert into dailyrevmetric.listViewstracking (date, commercial_type, article_type, brand_type, gender, " +
					" list, pdp, cart ) values (?, ?, ?, ?, ?, ?, ?, ?)";*/
			
			session.execute(boundStatement.bind(listViews.getDate(), checkNull(listViews.getCommercial_type()), 
					checkNull(listViews.getArticle_type()), checkNull(listViews.getBrand_type()), checkNull(listViews.getBrand()), 
					checkNull(listViews.getGender()), listViews.getList(), listViews.getPdp(), listViews.getCart()
					));
			
		}
		
		logger.info("Completed tracking list views at : " + new Date());
		System.out.println("Completed tracking list views at : " + new Date());
	}
	
	///
	
	
	private void trackContribMetric(ResultSet rs, String formattedDate) throws Exception {

		logger.info("Starting tracking contribution metrics at : " + new Date());
		System.out.println("Starting tracking contribution metrics at : " + new Date());

		
		Map<String, RevTemp> pairMap = new HashMap<String, RevTemp>();
		
		double revTot = 0;
		double tdTot = 0;
		double cdTot = 0;
		double rgmTot = 0;		
		
		int i = 0;
		
		//
		
		/*Set<String> commercial_type_Set = new TreeSet<String>();
		Set<String> article_type_Set = new TreeSet<String>();
		Set<String> brand_type_Set = new TreeSet<String>();
		Set<String> brand_Set = new TreeSet<String>();
		Set<String> gender_Set = new TreeSet<String>();*/
		
		//
		
		while (rs.next()) {
			i += 1;
			//System.out.println("in rs : " + rs.getFloat("revenue") + " , " + rs.getFloat("td") + " , " + rs.getFloat("rgm") + " , "
				//	+ rs.getString("commercial_type") + " , " + rs.getString("article_type") + " , " + rs.getString("gender"));
			
			double revenue = rs.getDouble("revenue");
			double td = rs.getDouble("td");
			double cd = rs.getDouble("cd");
			double rgm = rs.getDouble("rgm");
			long qtySold = rs.getLong("qtySold");
			
			revTot += revenue;
			tdTot += td;
			cdTot += cd;
			rgmTot += rgm;
			
			String commercial_type = rs.getString("commercial_type");
			String article_type = rs.getString("article_type");
			String brand_type = rs.getString("brand_type");
			String brand = rs.getString("brand");
			String gender = rs.getString("gender");
			
			String key = commercial_type + "|" + article_type  + "|" + brand_type  + "|" + brand  + "|" + gender;
			
//
			/*if (commercial_type != null) 
			commercial_type_Set.add(commercial_type);
			article_type_Set.add(article_type);
			brand_type_Set.add(brand_type);
			brand_Set.add(brand);
			gender_Set.add(gender);*/
			
			//
			
			RevTemp revTemp = null;
			
			if (pairMap.containsKey(key)) {
				revTemp = pairMap.get(key);
				revTemp.setRevenue(revTemp.getRevenue() + revenue);
				revTemp.setTd(revTemp.getTd() + td);
				revTemp.setCd(revTemp.getCd() + cd);
				revTemp.setRgm(revTemp.getRgm() + rgm);
				revTemp.setQtySold(revTemp.getQtySold() + qtySold);
			} else {
				revTemp = new RevTemp();
				revTemp.setCommercial_type(commercial_type);
				revTemp.setArticle_type(article_type);
				revTemp.setBrand_type(brand_type);
				revTemp.setBrand(brand);
				revTemp.setGender(gender);
				revTemp.setRevenue(revenue);
				revTemp.setTd(td);
				revTemp.setCd(cd);
				revTemp.setRgm(rgm);
				revTemp.setQtySold(qtySold);
			}
			pairMap.put(key, revTemp);
		}
		
		//
		
		
		//CreateDropdownJSON createDropdownJSON = new CreateDropdownJSON();
		
		/*createDropdownJSON.setCommercial_types(commercial_type_Set);
		createDropdownJSON.setArticle_types(article_type_Set);
		createDropdownJSON.setBrand_types(brand_type_Set);
		createDropdownJSON.setBrands(brand_Set);
		createDropdownJSON.setGenders(gender_Set);

		org.codehaus.jackson.map.ObjectMapper mapper = new org.codehaus.jackson.map.ObjectMapper();
		
		String result = "";
		try {
			result = mapper.writeValueAsString(createDropdownJSON);
		} catch (org.codehaus.jackson.JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (org.codehaus.jackson.map.JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//System.out.println(result);
		
		//
		
		
		//System.out.println("Records fetched : " + i);
		
		
		List<Revenue> revList = new ArrayList<Revenue>();

		Revenue rev = null;
		
		for ( Entry<String, RevTemp> entry : pairMap.entrySet()) {
			
			RevTemp revTemp = entry.getValue();
			rev = new Revenue();
			rev.setRevTot(revTot);
			rev.setTdTot(tdTot);
			rev.setCdTot(cdTot);
			rev.setRgmTot(rgmTot);
			
			int formattedDateInt = Integer.parseInt(formattedDate);
			
			rev.setDate(formattedDateInt);
			rev.setCommercial_type(revTemp.getCommercial_type());
			rev.setArticle_type(revTemp.getArticle_type());
			rev.setBrand_type(revTemp.getBrand_type());
			rev.setBrand(revTemp.getBrand());
			rev.setGender(revTemp.getGender());
			rev.setRev(revTemp.getRevenue());
			rev.setTd(revTemp.getTd());
			rev.setCd(revTemp.getCd());
			rev.setRgm(revTemp.getRgm());
			rev.setQtysold(revTemp.getQtySold());
			rev.setRev_cPerc(100*revTemp.getRevenue()/revTot);
			rev.setTd_cPerc(100*revTemp.getTd()/tdTot);
			rev.setCd_cPerc(100*revTemp.getCd()/cdTot);
			rev.setRgm_cPerc(100*revTemp.getRgm()/rgmTot);
			revList.add(rev);
		}
		
		saveToDB(revList);
		
		logger.info("Completed tracking contribution metrics at : " + new Date());
		System.out.println("Completed tracking contribution metrics at : " + new Date());
	}
	
	
	
	
	
	private void saveToDB(List<Revenue> revList) {
		
		cassandraConnector.connect(cassandraIP, 9042);
		Session session = cassandraConnector.getSession();
		
		
		com.datastax.driver.core.PreparedStatement preparedStatement = session.prepare(insertSql);
		
		BoundStatement boundStatement = new BoundStatement(preparedStatement);

		for (Revenue revenue : revList) {
			//System.out.println(revenue.getDate());
			session.execute(boundStatement.bind(revenue.getDate(), revenue.getRevTot(), revenue.getTdTot(),
					revenue.getCdTot(), revenue.getRgmTot(), checkNull(revenue.getCommercial_type()), 
					checkNull(revenue.getArticle_type()), checkNull(revenue.getBrand_type()), checkNull(revenue.getBrand()), 
					checkNull(revenue.getGender()), revenue.getRev(), revenue.getTd(), revenue.getCd(), 
					revenue.getRgm(), revenue.getRev_cPerc(), revenue.getTd_cPerc(), revenue.getCd_cPerc(), 
					revenue.getRgm_cPerc(), revenue.getQtysold()
					));
		}
		
	}
	
	private String checkNull(String str) {
		if (str == null) {
			return "NULL";
		} 
		return str;
	}
}
