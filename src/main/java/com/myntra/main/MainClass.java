package com.myntra.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Session;
import com.myntra.sql.connector.CassandraConnector;
import com.myntra.sql.connector.RedShiftConnectionFactory;
import com.myntra.vo.CreateDropdownJSON;
import com.myntra.vo.ListViews;
import com.myntra.vo.RevTemp;
import com.myntra.vo.Revenue;

public class MainClass {


	// 3 + brandtype + date range
	/*static String sql = "select a.*, (a.revenue - a.tax - a.cost + a.Vendor_funding +extra_charges) as RGM " +
						" from (select fo.order_created_date, dp.style_id, dp.commercial_type, dp.brand_type, dp.brand, dp.article_type, " +
						" dp.gender, sum(item_revenue_inc_cashback) as revenue, sum(quantity) as qtySold, " +
						" sum(article_mrp*quantity) as Article_MRP, Sum(fo.product_discount) as TD, " +
						" sum(vendor_funding) as Vendor_funding, sum(fo.Coupon_discount) as CD, " +
						" (max(o.shipping_charges)+max(o.emi_charges)+max(o.gift_charges)) as extra_charges, " + 
						" sum(fo.tax) as tax, sum(item_purchase_price_inc_tax) as cost from " +
						" fact_orderitem fo, dim_product dp, fact_order o " +
						" where fo.idproduct = dp.id and fo.order_id=o.order_id and " +
						" ( fo.order_created_date = %s ) and ( fo.is_realised =1 or fo.is_shipped = 1 ) " +
						" group by fo.order_created_date, dp.style_id, dp.commercial_type, dp.brand_type, dp.brand, dp.article_type, dp.gender ) as a ";*/

	static String listViewsSql = "select s.style_id, sum(list_count) List, sum(pdp_count) PDP, " +
			" sum(add_to_cart_count) Cart, sum(unit_sold) qtySold " +
			" from fact_basic_funnel_details_snapshot s " +
			" where s.load_date in (%s) " +
			" group by s.style_id ";

	static String skuSql = "select sku_id, style_id, brand, brand_type, article_type, gender, sum(quantity) as quantity " +
			" from bidb.fact_core_item " +
			" where order_created_date in (%s) and (is_shipped=1 or is_realised =1) and store_id = 1" +
			" group by sku_id, style_id, brand, brand_type, article_type, gender ";

	static String brokenSql = "select style_id, sku_id, sum(is_live_on_portal) as isLive from fact_product_snapshot " +
			" where date in (%s) " +
			" group by style_id, sku_id ";


	//CassandraConnector cassandraConnector = new CassandraConnector();
	//final String[] cassandraIP = {"127.0.0.1"};
	//final String[] cassandraIP = {"10.175.146.116"};

	private static Logger logger = LoggerFactory.getLogger(MainClass.class);

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) {

		try {
			MainClass mainClass = new MainClass();
			mainClass.run(args);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Process completed");
		}
		System.exit(0);
	}

	public void run(String[] args) throws Exception{
		// TODO Auto-generated method stub

		Connection connection = RedShiftConnectionFactory.getConnection();

		Statement statement = connection.createStatement();

		//Map<String, ListViews> beforeMap = null;
		Map<String, ListViews> afterMap = null;

		Map<String, ListViews> beforeSkuMap = null;

		if (statement != null) {

			String formattedDate = "20160427,20160428,20160429,20160502,20160503,20160504,20160505";
			//String formattedDate = "20160427,20160428,20160429,20160430,20160501,20160502,20160503,20160504,20160505";

			//String formattedDate = "20160510,20160511,20160512,20160513,20160516,20160517,20160518";
			
			ResultSet rs1 = statement.executeQuery(String.format(listViewsSql, formattedDate));
			Map<String, ListViews> beforeMap = trackListViews(rs1, formattedDate);

			ResultSet rs2 = statement.executeQuery(String.format(skuSql, formattedDate));
			beforeSkuMap = trackskuData(rs2, formattedDate);

			ResultSet rs3 = statement.executeQuery(String.format(brokenSql, formattedDate));
			Map<String, ListViews> beforebrokenMap = trackbrokenData(rs3, formattedDate);


			for(Map.Entry<String, ListViews> entry : beforeSkuMap.entrySet()) {
				ListViews vo = entry.getValue();
				String styleId = vo.getStyle_id();
				String skuId = vo.getSku_id();

				ListViews styleDetails = beforeMap.get(styleId);
				if (styleDetails != null) {
					vo.setList(styleDetails.getList());
					vo.setPdp(styleDetails.getPdp());
					vo.setCart(styleDetails.getCart());
					vo.setStyleQtySold(styleDetails.getStyleQtySold());
				}

				ListViews skuDetails = beforebrokenMap.get(skuId);
				if (skuDetails != null) {
					vo.setIslive(skuDetails.getIslive());
				}

				beforeSkuMap.put(skuId, vo);
			}




			/*formattedDate = "20160510,20160511,20160512,20160513,20160516,20160517,20160518";
			//formattedDate = "20160510,20160511,20160512,20160513,20160514,20160515,20160516,20160517,20160518";

			ResultSet rs2 = statement.executeQuery(String.format(listViewsSql, formattedDate));
			afterMap = trackListViews(rs2, formattedDate);*/

		}

		writeToFileSku(beforeSkuMap);

		//List<String> styleList = readFile("./src/UnBroken");
		//List<String> styleList = readFile("./src/UnBroken");


		// write to file start


		//writeToFile(beforeMap, afterMap, styleList);

		statement.close();
		connection.close();

		if(statement != null) {
			statement.close();
			logger.info("Closing statement object");
		}
		if(connection != null) {
			connection.close();
			logger.info("Closing Connection object");
		}
	}


	private void writeToFileSku(Map<String, ListViews> beforeSkuMap) {

		String header = "StyleId,SkuId,islive,StyleQtySold,SkuQtySold,list,pdp,cart,articleType," +
				"brandType,brand,gender";

		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			//file = new File("/home/en-sorabhk/files/brokenSkuData.csv");
			file = new File("./brokenSkuData.csv");
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(header);

			for(Map.Entry<String, ListViews> entry : beforeSkuMap.entrySet()) {
				ListViews vo = entry.getValue();
				bufferedWriter.write("\n");
				bufferedWriter.write(vo.getStyle_id()+","+vo.getSku_id()+","+vo.getIslive()+","+vo.getStyleQtySold()
						+","+vo.getSkuQtySold()+","+vo.getList()+","+vo.getPdp()+","+vo.getCart()+","+vo.getArticle_type()
						+","+vo.getBrand_type()+","+vo.getBrand()+","+vo.getGender());

			}
		} catch (IOException e) {
			System.out.println("Exception raised while writing to report : " + e);
			e.printStackTrace();
			//logger.error("Exception raised while writing to report : " + e);
		} 
		finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception raised while closing writer objects : "+ e);
				//logger.error("Exception raised while closing writer objects : "+ e);
				e.printStackTrace();
			} 
		}

	}

	private Map<String, ListViews> trackbrokenData(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		//style_id, sku_id, is_live_on_portal

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();

			//int formattedDateInt = Integer.parseInt(formattedDate);
			//listViews.setDate(formattedDateInt);
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setSku_id(rs.getString("sku_id"));
			//listViews.setIslive(rs.getInt("is_live_on_portal"));
			listViews.setIslive(rs.getInt("isLive"));

			styleMap.put(listViews.getSku_id(), listViews);
		}
		return styleMap;
	}

	private Map<String, ListViews> trackskuData(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		//select sku_id, style_id, brand, brand_type, article_type, gender, quantity, order_created_date

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();

			//int formattedDateInt = Integer.parseInt(formattedDate);
			//listViews.setDate(formattedDateInt);
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setSku_id(rs.getString("sku_id"));
			//listViews.setCommercial_type(rs.getString("commercial_type"));
			listViews.setArticle_type(rs.getString("article_type"));
			listViews.setBrand_type(rs.getString("brand_type"));
			listViews.setBrand(rs.getString("brand"));
			listViews.setGender(rs.getString("gender"));
			//listViews.setList(rs.getLong("list"));
			//listViews.setPdp(rs.getLong("pdp"));
			//listViews.setCart(rs.getLong("cart"));
			//listViews.setStyleQtySold(rs.getLong("qtySold"));
			listViews.setSkuQtySold(rs.getLong("quantity"));
			styleMap.put(listViews.getSku_id(), listViews);
		}
		return styleMap;
	}

	/*private void writeToFile(Map<String, ListViews> beforeMap, Map<String, ListViews> afterMap, List<String> styleList) {

		String header = "style,beforeList,afterList,beforePdp,afterPdp,beforeCart,afterCart,beforeQtySold,afterQtySold" +
				",beforeConversion(pdp/list),afterConversion(pdp/list),conversionDiff(pdp/list)" +
				",beforeConversion(cart/pdp),afterConversion(cart/pdp),conversionDiff(cart/pdp)" +
				",beforeConversion(qtySold/pdp),afterConversion(qtySold/pdp),conversionDiff(qtySold/pdp)";

		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			file = new File("/home/en-sorabhk/files/unbroken_weekdays.csv");
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(header);

			for (String style : styleList) {
				ListViews before = beforeMap.get(style);
				ListViews after = afterMap.get(style);

				if(before != null && after != null) {
					bufferedWriter.write("\n");

					double bfrConvPdpList = (before.getList()!=0)?((double)before.getPdp()/(double)before.getList()):0;
					double aftConvPdpList = (after.getList()!=0)?((double)after.getPdp()/(double)after.getList()):0;

					double bfrConvCartPdp = (before.getPdp()!=0)?((double)before.getCart()/(double)before.getPdp()):0;
					double aftConvCartPdp = (after.getPdp()!=0)?((double)after.getCart()/(double)after.getPdp()):0;

					double bfrConvQtyPdp = (before.getPdp()!=0)?((double)before.getQtySold()/(double)before.getPdp()):0;
					double aftConvQtyPdp = (after.getPdp()!=0)?((double)after.getQtySold()/(double)after.getPdp()):0;

					bufferedWriter.write(style + "," + before.getList() + "," + after.getList() + 
							"," + before.getPdp() + "," + after.getPdp() + 
							"," + before.getCart() + "," + after.getCart() + 
							"," + before.getQtySold() + "," + after.getQtySold() +

							"," + bfrConvPdpList + "," + aftConvPdpList +
							"," + (bfrConvPdpList - aftConvPdpList) +

							"," + bfrConvCartPdp + "," + aftConvCartPdp +
							"," + (bfrConvCartPdp - aftConvCartPdp) +

							"," + bfrConvQtyPdp + "," + aftConvQtyPdp + 
							"," + (bfrConvQtyPdp - aftConvQtyPdp)
							);
				}
			}



			for(Map.Entry<String, StyleDetails> entry : dataMap.entrySet()) {
				StyleDetails val = entry.getValue();
				if (val!= null ) {
					bufferedWriter.write("\n");
					bufferedWriter.write(val.getStyleid() +", " + val.getCurrentTd()+", " + val.getNewTd()
							+", " + val.getMinDiscount()+", " + val.getMaxDiscount()+", " + val.getNorm_adj_bm_new_actual()+", " + val.getTotalscore()
							+", " + val.getDmdfinal()+", " + val.getArticlemrp()+", " + val.getArticletype()
							+", " + val.getGender()+", " + val.getDiscountfunding()
							+", " + val.getFundingpercentage()+", " + val.getDiscountlimit()
							+", " + val.getFrom_unixtime_expired_on()+", " + val.getGetcount()
							+", " + val.getGetamount()+", " + val.getBrand()+", " + val.getDiscountid()
							+", " + val.getBrandtype()+", " + val.getBusinessunit()+", " + val.getCurrentcommercialtype()
							+", " + val.getAverageage()
							+", " + val.getHealth()+", " + val.getNorm_adj_bm_breach()
							//+", " + val.getNewDemand()+", " + val.getRankingFactor()
							);
				} else {
					//logger.info("Data not found for style : " + val.getStyleid());
					System.out.println("Data not found for style : " + val.getStyleid());
				}
			}
		} catch (IOException e) {
			System.out.println("Exception raised while writing to report : " + e);
			e.printStackTrace();
			//logger.error("Exception raised while writing to report : " + e);
		} 
		finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception raised while closing writer objects : "+ e);
				//logger.error("Exception raised while closing writer objects : "+ e);
				e.printStackTrace();
			} 
		}

	}*/

	private static List<String> readFile(String fileName) {

		List<String> styleList = new ArrayList<String>();

		File file = new File(fileName);

		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String str;
		try {
			while ((str = in.readLine()) != null) {
				styleList.add(str);
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		return styleList;
	}

	private Map<String, ListViews> trackListViews(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();

			//int formattedDateInt = Integer.parseInt(formattedDate);
			//listViews.setDate(formattedDateInt);
			listViews.setStyle_id(rs.getString("style_id"));
			//listViews.setCommercial_type(rs.getString("commercial_type"));
			//listViews.setArticle_type(rs.getString("article_type"));
			//listViews.setBrand_type(rs.getString("brand_type"));
			//listViews.setBrand(rs.getString("brand"));
			//listViews.setGender(rs.getString("gender"));
			listViews.setList(rs.getLong("list"));
			listViews.setPdp(rs.getLong("pdp"));
			listViews.setCart(rs.getLong("cart"));
			listViews.setStyleQtySold(rs.getLong("qtySold"));
			styleMap.put(listViews.getStyle_id(), listViews);
		}
		return styleMap;
	}


	private String checkNull(String str) {
		if (str == null) {
			return "NULL";
		} 
		return str;
	}
}
