package com.myntra.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Session;
import com.myntra.sql.connector.CassandraConnector;
import com.myntra.sql.connector.RedShiftConnectionFactory;
import com.myntra.vo.CreateDropdownJSON;
import com.myntra.vo.ElasticityRackVo;
import com.myntra.vo.ListViews;
import com.myntra.vo.RackValVO;
import com.myntra.vo.RevTemp;
import com.myntra.vo.Revenue;
import com.myntra.vo.StyleValVO;

public class RackCompleteClass {


	static String listViewsSql = "select s.style_id, sum(list_count) List, sum(pdp_count) PDP, " +
			" sum(add_to_cart_count) Cart, sum(unit_sold) qtySold " +
			" from fact_basic_funnel_details_snapshot s " +
			" where s.load_date in (%s) " +
			" group by s.style_id ";

	static String skuSql = "select sku_id, style_id, brand, brand_type, article_type, gender, sum(quantity) as quantity " +
			" from bidb.fact_core_item " +
			" where order_created_date in (%s) and (is_shipped=1 or is_realised =1) and store_id = 1" +
			" group by sku_id, style_id, brand, brand_type, article_type, gender ";

	static String brokenSql = "select style_id, sku_id, sum(is_live_on_portal) as isLive from fact_product_snapshot " +
			" where date in (%s) " +
			" group by style_id, sku_id ";


	static Map<String, ListViews> styleInfoMap = new HashMap<String, ListViews>();


	static long beforePdpTotal = 0;
	static long afterPdpTotal = 0;

	private static CassandraConnector cassandraConnector = new CassandraConnector();
	final static String[] cassandraIP = {"10.175.146.116","10.162.45.12","10.151.2.83"};

	private static Logger logger = LoggerFactory.getLogger(RackCompleteClass.class);

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws Exception{


		cassandraConnector.connect(cassandraIP, 9042);
		Session session = cassandraConnector.getSession();

		Connection connection = RedShiftConnectionFactory.getConnection();

		Statement statement = connection.createStatement();

		Map<String, ListViews> beforeSkuMap = run(statement, "20160427,20160428,20160429,20160502,20160503,20160504,20160505", "before");

		Map<String, ListViews> afterSkuMap = run(statement, "20160510,20160511,20160512,20160513,20160516,20160517,20160518", "after");

		Map<String, ElasticityRackVo> rackMap = RackReader.readRackInfo(session);

		//analyse(beforeSkuMap, afterSkuMap, rackMap);

		Map<String, StyleValVO> beforeStyleValMap = aggregateAtStyleLevel(beforeSkuMap);
		Map<String, RackValVO> beforeRackValMap = aggregateAtRackLevel(beforeStyleValMap, rackMap);
		
		Map<String, StyleValVO> afterStyleValMap = aggregateAtStyleLevel(afterSkuMap);
		Map<String, RackValVO> afterRackValMap = aggregateAtRackLevel(afterStyleValMap, rackMap);

		analyse(beforeRackValMap, afterRackValMap);



		statement.close();
		connection.close();

		if(statement != null) {
			statement.close();
			logger.info("Closing statement object");
		}
		if(connection != null) {
			connection.close();
			logger.info("Closing Connection object");
		}





		/*try {
			CompleteClass mainClass = new CompleteClass();
			mainClass.run(args);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Process completed");
		}*/
		System.exit(0);
	}

	public static Map<String, ListViews> run(Statement statement, String formattedDate, String timeStatus) throws Exception {
		// TODO Auto-generated method stub

		Map<String, ListViews> beforeSkuMap = null;

		if (statement != null) {

			ResultSet rs1 = statement.executeQuery(String.format(listViewsSql, formattedDate));
			Map<String, ListViews> beforeMap = trackListViews(rs1, formattedDate, timeStatus);

			ResultSet rs2 = statement.executeQuery(String.format(skuSql, formattedDate));
			beforeSkuMap = trackskuData(rs2, formattedDate);

			ResultSet rs3 = statement.executeQuery(String.format(brokenSql, formattedDate));
			Map<String, ListViews> beforebrokenMap = trackbrokenData(rs3, formattedDate);


			for(Map.Entry<String, ListViews> entry : beforeSkuMap.entrySet()) {
				ListViews vo = entry.getValue();
				String styleId = vo.getStyle_id();
				String skuId = vo.getSku_id();

				ListViews styleDetails = beforeMap.get(styleId);
				if (styleDetails != null) {
					vo.setList(styleDetails.getList());
					vo.setPdp(styleDetails.getPdp());
					vo.setCart(styleDetails.getCart());
					vo.setStyleQtySold(styleDetails.getStyleQtySold());
				}

				ListViews skuDetails = beforebrokenMap.get(skuId);
				if (skuDetails != null) {
					vo.setIslive(skuDetails.getIslive());
				}

				beforeSkuMap.put(skuId, vo);
				styleInfoMap.put(styleId, vo);
			}
		}
		return beforeSkuMap;
	}

	private static Map<String, ListViews> trackbrokenData(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setSku_id(rs.getString("sku_id"));
			listViews.setIslive(rs.getInt("isLive"));
			styleMap.put(listViews.getSku_id(), listViews);
		}
		return styleMap;
	}

	private static Map<String, ListViews> trackskuData(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setSku_id(rs.getString("sku_id"));
			listViews.setArticle_type(rs.getString("article_type"));
			listViews.setBrand_type(rs.getString("brand_type"));
			listViews.setBrand(rs.getString("brand"));
			listViews.setGender(rs.getString("gender"));
			listViews.setSkuQtySold(rs.getLong("quantity"));
			styleMap.put(listViews.getSku_id(), listViews);
		}
		return styleMap;
	}

	private static Map<String, ListViews> trackListViews(ResultSet rs, String formattedDate, String timeStatus) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setList(rs.getLong("list"));
			listViews.setPdp(rs.getLong("pdp"));


			if (timeStatus.equals("before")) {
				beforePdpTotal += rs.getLong("pdp");
			} else if (timeStatus.equals("after")) {
				afterPdpTotal += rs.getLong("pdp");
			}

			listViews.setCart(rs.getLong("cart"));
			listViews.setStyleQtySold(rs.getLong("qtySold"));
			styleMap.put(listViews.getStyle_id(), listViews);
		}
		return styleMap;
	}

	private static Map<String, StyleValVO> aggregateAtStyleLevel(Map<String, ListViews> skuMap) {

		//style level sku data map
		Map<String, Map<String,ListViews>> styleMap = new HashMap<String, Map<String,ListViews>>();

		for (Map.Entry<String, ListViews> entry : skuMap.entrySet()) {
			ListViews vo = entry.getValue();
			if (styleMap.containsKey(vo.getStyle_id())) {
				Map<String,ListViews> map = styleMap.get(vo.getStyle_id());
				map.put(vo.getSku_id(), vo);
				styleMap.put(vo.getStyle_id(), map);
			} else {
				Map<String,ListViews> map = new HashMap<String, ListViews>();
				map.put(vo.getSku_id(), vo);
				styleMap.put(vo.getStyle_id(), map);
			}
		}

		Map<String, StyleValVO> styleValueMap = new HashMap<String, StyleValVO>();


		for (Map.Entry<String, Map<String,ListViews>> entry : styleMap.entrySet()) {

			String style = entry.getKey();
			Map<String, ListViews> styleSkuMap = entry.getValue();

			int maxIsLive = 0;
			
			double styleVal = 0;
			int skuCount = 0;
			long pdp = 0;
			String article_type = null;
			String brand_type = null;
			String brand = null;
			String gender = null;
			
			for (Map.Entry<String, ListViews> entry2 : styleSkuMap.entrySet()) {
				ListViews skuVO = entry2.getValue();
				if (skuVO != null && skuVO.getIslive() != 0) {
					if(skuVO.getIslive() > maxIsLive) {
						maxIsLive = skuVO.getIslive();
					}
					article_type = skuVO.getArticle_type();
					brand_type = skuVO.getBrand_type();
					brand = skuVO.getBrand();
					gender = skuVO.getGender();
					pdp += skuVO.getPdp();
					skuCount++;
					styleVal += ((double)skuVO.getSkuQtySold()/(double)skuVO.getIslive());
				}
			}
			
			StyleValVO vo = new StyleValVO();
			vo.setPdpTotal(pdp);
			vo.setPdpPerDay((double)pdp/maxIsLive);
			vo.setQtySoldNormalised(styleVal);
			vo.setSkuCount(skuCount);
			vo.setArticle_type(article_type);
			vo.setBrand_type(brand_type);
			vo.setBrand(brand);
			vo.setGender(gender);
			styleValueMap.put(style, vo);
		}

		return styleValueMap;
	}
	
	
	
	private static Map<String, RackValVO> aggregateAtRackLevel(Map<String, StyleValVO> beforeStyleValMap, Map<String, ElasticityRackVo> rackMap) {
		
		Map<String, RackValVO> map = new HashMap<String, RackValVO>();
		
		for(Map.Entry<String, StyleValVO> entry : beforeStyleValMap.entrySet()){
			String style = entry.getKey();
			StyleValVO vo = entry.getValue();
			
			ElasticityRackVo rackVo = rackMap.get(style);
			if (rackVo != null) {			
				//String key = vo.getArticle_type() +","+ vo.getGender() +","+ rackVo.getRack() +","+ rackVo.getCollection();
				String key = vo.getArticle_type() +"|"+ vo.getGender() +"|"+ rackVo.getRack() +"|"+ rackVo.getCollection();
				if (map.containsKey(key)) {
					RackValVO rackValVo = map.get(key);
					rackValVo.setPdpAbsolute(rackValVo.getPdpAbsolute() + vo.getPdpTotal());
					rackValVo.setPdpPerDay(rackValVo.getPdpPerDay() + vo.getPdpPerDay());
					rackValVo.setQtySoldNormalised(rackValVo.getQtySoldNormalised() + vo.getQtySoldNormalised());
					rackValVo.setStyleCount(rackValVo.getStyleCount() + 1);
					map.put(key, rackValVo);
				} else {
					RackValVO rackValVo = new RackValVO();
					rackValVo.setArticle_type(vo.getArticle_type());
					rackValVo.setGender(vo.getGender());
					rackValVo.setRack(rackVo.getRack());
					rackValVo.setCollection(rackVo.getCollection());
					rackValVo.setPdpAbsolute(vo.getPdpTotal());
					rackValVo.setPdpPerDay(vo.getPdpPerDay());
					rackValVo.setQtySoldNormalised(vo.getQtySoldNormalised());
					rackValVo.setStyleCount(1);
					map.put(key, rackValVo);
				}
			} else {
				String key = vo.getArticle_type() +"|"+ vo.getGender() +"|"+ null;
				if (map.containsKey(key)) {
					RackValVO rackValVo = map.get(key);
					rackValVo.setPdpAbsolute(rackValVo.getPdpAbsolute() + vo.getPdpTotal());
					rackValVo.setPdpPerDay(rackValVo.getPdpPerDay() + vo.getPdpPerDay());
					rackValVo.setQtySoldNormalised(rackValVo.getQtySoldNormalised() + vo.getQtySoldNormalised());
					rackValVo.setStyleCount(rackValVo.getStyleCount() + 1);
					map.put(key, rackValVo);
				} else {
					RackValVO rackValVo = new RackValVO();
					rackValVo.setArticle_type(vo.getArticle_type());
					rackValVo.setGender(vo.getGender());
					rackValVo.setRack(null);
					rackValVo.setCollection(null);
					rackValVo.setPdpAbsolute(vo.getPdpTotal());
					rackValVo.setPdpPerDay(vo.getPdpPerDay());
					rackValVo.setQtySoldNormalised(vo.getQtySoldNormalised());
					rackValVo.setStyleCount(1);
					map.put(key, rackValVo);
				}
			}
			
			
		}
		
		return map;
		
	}

	

	private static void analyse(Map<String, RackValVO> beforeRackValMap, Map<String, RackValVO> afterRackValMap) {

		int equalCount = 0;
		int positiveCount = 0;
		int negativeCount = 0;

		List<String> equalList = new ArrayList<String>();
		List<String> positiveList = new ArrayList<String>();
		List<String> negativeList = new ArrayList<String>();

		int pdpIncQtyInc = 0;
		int pdpDecQtyInc = 0;
		int pdpIncQtyDec = 0;
		int pdpDecQtyDec = 0;

		List<String> pdpIncQtyIncList = new ArrayList<String>();
		List<String> pdpDecQtyIncList = new ArrayList<String>();
		List<String> pdpIncQtyDecList = new ArrayList<String>();
		List<String> pdpDecQtyDecList = new ArrayList<String>();

		
		for (Map.Entry<String, RackValVO> entry : afterRackValMap.entrySet()) {

			String rackStr = entry.getKey();

			RackValVO afterRackVo = entry.getValue();
			RackValVO beforeRackVo = beforeRackValMap.get(rackStr);

			System.out.println("**********************************************");

			//int totalQtySoldBefore = 0;
			//int totalQtySoldAfter = 0;


			if (beforeRackVo != null) {
				
				double beforeConversion = (beforeRackVo.getQtySoldNormalised()/beforeRackVo.getPdpPerDay());
		    	double afterConversion = (afterRackVo.getQtySoldNormalised()/afterRackVo.getPdpPerDay());

				double convChange = (100/beforeConversion)*afterConversion;

				String ConvVal = "";
				if (convChange < 120 && convChange > 80) {
					ConvVal = "equal";
					equalCount++;
					equalList.add(rackStr);
				} else if (convChange > 120) {
					ConvVal = "positive";
					positiveCount++;
					positiveList.add(rackStr);
				} else if (convChange < 80) {
					ConvVal = "negative";
					negativeCount++;
					negativeList.add(rackStr);
				}


				System.out.println(rackStr+" ### "+convChange+" ### "+beforeConversion+" *** "+afterConversion+" ### "+beforePdpTotal+" *** "+afterPdpTotal+" ### "+beforeRackVo.getQtySoldNormalised()+" *** "+afterRackVo.getQtySoldNormalised()+" ### "+ConvVal);

				double beforePdpContribution = (double)beforeRackVo.getPdpAbsolute()/((double)beforePdpTotal/1000);
				double afterPdpContribution = (double)afterRackVo.getPdpAbsolute()/((double)afterPdpTotal/1000);

				double pdpContributionChange = (100/beforePdpContribution)*afterPdpContribution;

				if (pdpContributionChange > 120 && ConvVal.equals("positive")) {
					pdpIncQtyInc++;
					pdpIncQtyIncList.add(rackStr);
				}
				if (pdpContributionChange > 120 && ConvVal.equals("negative")) {
					pdpIncQtyDec++;
					pdpIncQtyDecList.add(rackStr);
				}
				if (pdpContributionChange < 80 && ConvVal.equals("positive")) {
					pdpDecQtyInc++;
					pdpDecQtyIncList.add(rackStr);
				}
				if (pdpContributionChange < 80 && ConvVal.equals("negative")) {
					pdpDecQtyDec++;
					pdpDecQtyDecList.add(rackStr);
				}
			}
		}

		System.out.println("**********************************************");

		System.out.println("Equal : " + equalCount +"  positive : "+ positiveCount + " negative : " + negativeCount);
		System.out.println(pdpIncQtyInc + "    " + pdpIncQtyDec + "    " + pdpDecQtyInc + "   " + pdpDecQtyDec);


		System.out.println("pdpIncQtyIncList  :  "+ pdpIncQtyIncList);
		System.out.println("pdpDecQtyIncList  :  "+ pdpDecQtyIncList);
		System.out.println("pdpIncQtyDecList  :  "+ pdpIncQtyDecList);
		System.out.println("pdpDecQtyDecList  :  "+ pdpDecQtyDecList);


		/* System.out.println("equalList : " + equalList);
	    System.out.println("positiveList : " + positiveList);
	    System.out.println("negativeList : " + negativeList);

	    System.out.println("#################################################");

	    for (String sty : positiveList) {
	    	if (!pdpIncQtyIncStr.contains(sty) && !pdpDecQtyIncStr.contains(sty)) {
	    		System.out.println(sty);
	    	}
	    }*/

		/* cassandraConnector.connect(cassandraIP, 9042);
		Session session = cassandraConnector.getSession();

		Map<String, ElasticityRackVo> rackMap = RackReader.readRackInfo(session);

		//ComputedSnapshotReader.readComputedSnapshot(session, currentDate, currentTime)

	    categoryLevelAnalysis(pdpIncQtyIncList, rackMap, "pdpIncQtyIncList");
	    categoryLevelAnalysis(pdpDecQtyIncList, rackMap, "pdpDecQtyIncList");
	    categoryLevelAnalysis(pdpIncQtyDecList, rackMap, "pdpIncQtyDecList");
	    categoryLevelAnalysis(pdpDecQtyDecList, rackMap, "pdpDecQtyDecList");*/

	}


	private static void categoryLevelAnalysis(List<String> list, Map<String, ElasticityRackVo> rackMap, String fileName) {

		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			file = new File("./"+fileName+".csv");
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write("style,articleType,gender,elasticity,rack,collection,cascElasticity_CD,cascElasticity_TD,cascElasticity_unif,cascVisibility_Elasticity");
			for (String style : list) {
				bufferedWriter.write("\n");
				ListViews styleInfoVO = styleInfoMap.get(style);
				String brand = styleInfoVO.getBrand();
				String articleType = styleInfoVO.getArticle_type();
				String gender = styleInfoVO.getGender();

				ElasticityRackVo vo = rackMap.get(style);

				if (vo != null) {
					//bufferedWriter.write(style+","+vo.getArticleType()+","+vo.getGender()+","+vo.getElasticity()+","+vo.getRack()+","+vo.getCollection()+","+vo.getCascElasticity_CD()+","+vo.getCascElasticity_TD()+","+vo.getCascElasticity_unif()+","+vo.getCascVisibility_Elasticity());
					bufferedWriter.write(style+","+articleType+","+gender+","+brand+","+vo.getElasticity()+","+vo.getRack()+","+vo.getCollection()+","+vo.getCascElasticity_CD()+","+vo.getCascElasticity_TD()+","+vo.getCascElasticity_unif()+","+vo.getCascVisibility_Elasticity());
				} else {
					//bufferedWriter.write(style+","+vo);
					bufferedWriter.write(style+","+articleType+","+gender+","+brand+","+vo);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 
		finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	}







}
