package com.myntra.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadRackFiles {
	
	public static void main(String[] args) {
		
		
		Map<String,List<String>> map = new HashMap<String, List<String>>();
		
		//File file = new File("/home/en-sorabhk/Documents/newFiles/pdpIncQtyIncList.csv");
		File file = new File("/home/en-sorabhk/Documents/newFiles/pdpDecQtyIncList.csv");
		//File file = new File("/home/en-sorabhk/Documents/newFiles/pdpIncQtyDecList.csv");
		//File file = new File("/home/en-sorabhk/Documents/newFiles/pdpDecQtyDecList.csv");
		
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String str;
	    try {
			String[] strArr = null;
			while ((str = in.readLine()) != null) {
				strArr = str.split(",");
				if (!strArr[0].equalsIgnoreCase("style")) {
					if (strArr.length > 2) {
						//String category = strArr[1]+","+strArr[2]+","+strArr[4]+","+strArr[5];
						String category = strArr[1]+","+strArr[2];
						String rack = null;
						if (strArr.length == 11) {
							rack = strArr[5]+","+strArr[6];
						}
						String brand = strArr[3];
						String key = category+","+brand;
						//System.out.println(strArr[0]+","+strArr[1]+","+strArr[2]+","+strArr[3]+","+strArr[4]+","+strArr[5]+","+strArr[6]+","+strArr[7]+","+strArr[8]+","+strArr[9]);
						if (map.containsKey(key)) {
							List<String> list = map.get(key);
							list.add(strArr[0]);
							map.put(key, list);
						} else {
							List<String> list = new ArrayList<String>();
							list.add(strArr[0]);
							map.put(key, list);
						}
					} else {
						
					}
				}
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    System.out.println("############################################");
	    for (Map.Entry<String, List<String>> entry : map.entrySet()) {
	    	if (entry.getValue().size() > 10) {
	    	System.out.println(entry.getKey() + "      :      " + entry.getValue().size());
	    	}
	    }
	    System.out.println("############################################");
	}
	
}
