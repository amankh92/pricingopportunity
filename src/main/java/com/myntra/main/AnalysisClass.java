package com.myntra.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Session;
import com.myntra.sql.connector.CassandraConnector;
import com.myntra.sql.connector.RedShiftConnectionFactory;
import com.myntra.vo.CreateDropdownJSON;
import com.myntra.vo.ExcelVO;
import com.myntra.vo.ListViews;
import com.myntra.vo.RevTemp;
import com.myntra.vo.Revenue;

public class AnalysisClass {

	public static void main(String[] args) {

		try {
			AnalysisClass mainClass = new AnalysisClass();
			mainClass.run(args);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Process completed");
		}
		System.exit(0);
	}

	public void run(String[] args) throws Exception{
		
		String findStyle = null;
		
		Map<String, Map<String,ListViews>> beforeMap = new HashMap<String, Map<String,ListViews>>();
		
		File file = new File("/home/en-sorabhk/files/brokenSkuData_before.csv");
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String str;
	    try {
			String[] strArr = null;
			while ((str = in.readLine()) != null) {
				strArr = str.split(",");
				if (!strArr[0].equalsIgnoreCase("StyleId")) {
					ListViews vo = new ListViews();
					vo.setStyle_id(strArr[0]);
					vo.setSku_id(strArr[1]);
					vo.setIslive(Integer.parseInt(strArr[2]));
					vo.setStyleQtySold(Long.parseLong(strArr[3]));
					vo.setSkuQtySold(Long.parseLong(strArr[4]));
					vo.setList(Long.parseLong(strArr[5]));
					vo.setPdp(Long.parseLong(strArr[6]));
					vo.setCart(Long.parseLong(strArr[7]));
					vo.setArticle_type(strArr[8]);
					vo.setBrand_type(strArr[9]);
					vo.setBrand(strArr[10]);
					vo.setGender(strArr[11]);
				    //map.put(vo.getStyle_id()+","+vo.getSku_id(), vo);
					if (beforeMap.containsKey(vo.getStyle_id())) {
						Map<String,ListViews> map = beforeMap.get(vo.getStyle_id());
						map.put(vo.getSku_id(), vo);
						beforeMap.put(vo.getStyle_id(), map);
					} else {
						Map<String,ListViews> map = new HashMap<String, ListViews>();
						map.put(vo.getSku_id(), vo);
						beforeMap.put(vo.getStyle_id(), map);
					}
			    }
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Map<String, Map<String,ListViews>> afterMap = new HashMap<String, Map<String,ListViews>>();
		
		File file1 = new File("/home/en-sorabhk/files/brokenSkuData_after.csv");
		BufferedReader in1 = null;
		try {
			in1 = new BufferedReader(new FileReader(file1));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String str1;
	    try {
			String[] strArr = null;
			while ((str1 = in1.readLine()) != null) {
				strArr = str1.split(",");
				if (!strArr[0].equalsIgnoreCase("StyleId")) {
					ListViews vo = new ListViews();
					vo.setStyle_id(strArr[0]);
					vo.setSku_id(strArr[1]);
					vo.setIslive(Integer.parseInt(strArr[2]));
					vo.setStyleQtySold(Long.parseLong(strArr[3]));
					vo.setSkuQtySold(Long.parseLong(strArr[4]));
					vo.setList(Long.parseLong(strArr[5]));
					vo.setPdp(Long.parseLong(strArr[6]));
					vo.setCart(Long.parseLong(strArr[7]));
					vo.setArticle_type(strArr[8]);
					vo.setBrand_type(strArr[9]);
					vo.setBrand(strArr[10]);
					vo.setGender(strArr[11]);
				    //map.put(vo.getStyle_id()+","+vo.getSku_id(), vo);
					if (afterMap.containsKey(vo.getStyle_id())) {
						Map<String,ListViews> map = afterMap.get(vo.getStyle_id());
						map.put(vo.getSku_id(), vo);
						afterMap.put(vo.getStyle_id(), map);
					} else {
						Map<String,ListViews> map = new HashMap<String, ListViews>();
						map.put(vo.getSku_id(), vo);
						afterMap.put(vo.getStyle_id(), map);
					}
			    }
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	    
	    for (Map.Entry<String, Map<String,ListViews>> entry : afterMap.entrySet()) {
	    	
	    	String style = entry.getKey();
	    	Map<String, ListViews> afterSkuMap = entry.getValue();
	    	Map<String, ListViews> beforeSkuMap = beforeMap.get(style);
	    	
	    	int totalQtySoldBefore = 0;
	    	int totalQtySoldAfter = 0;
	    	
	    	long beforePdpCount = 0;
	    	long afterPdpCount = 0;
	    	
	    	int beforeIsLiveMax = 1;
	    	int afterIsLiveMax = 1;
	    	
	    	if (beforeSkuMap!=null) {

	    		Set<String> skuSet = new HashSet<String>();
	    		
		    	for (Map.Entry<String, ListViews> skuEntry : beforeSkuMap.entrySet()) {
		    		ListViews val = skuEntry.getValue();
		    		beforePdpCount = val.getPdp();
		    		totalQtySoldBefore += val.getSkuQtySold();
		    		
		    		if (val.getIslive() > beforeIsLiveMax) {
		    			beforeIsLiveMax = val.getIslive();
		    		}
		    		skuSet.add(skuEntry.getKey());
		    	}
		    	
		    	for (Map.Entry<String, ListViews> skuEntry : afterSkuMap.entrySet()) {
		    		ListViews val = skuEntry.getValue();
		    		afterPdpCount = val.getPdp();
		    		totalQtySoldAfter += val.getSkuQtySold();
		    		if (val.getIslive() > afterIsLiveMax) {
		    			afterIsLiveMax = val.getIslive();
		    		}
		    		skuSet.add(skuEntry.getKey());
		    	}

		    	if (findStyle == null || style.equals(findStyle)) {
		    	System.out.println("  ********************************************  ");
		    	System.out.println(style + "  " + totalQtySoldBefore + "  " + totalQtySoldAfter);
		    	}
		    	
		    	int beforeSkuCount = 0;
		    	double beforeStyleVal = 0;
		    	
		    	int afterSkuCount = 0;
		    	double afterStyleVal = 0;
		    	
		    	
		    	
		    	for (String sku : skuSet) {
		    		
		    		ListViews afterSkuVO = afterSkuMap.get(sku);
		    		ListViews beforeSkuVO = beforeSkuMap.get(sku);
		    		
		    		if (beforeSkuVO != null && afterSkuVO != null) {
		    			if (beforeSkuVO.getIslive() != 0) {
		    				beforeSkuCount++;
		    				beforeStyleVal += ((double)beforeSkuVO.getSkuQtySold()/(double)beforeSkuVO.getIslive());
		    			}
		    			if (afterSkuVO.getIslive() != 0) {
		    				afterSkuCount++;
		    				afterStyleVal += ((double)afterSkuVO.getSkuQtySold()/(double)afterSkuVO.getIslive());
		    			}
		    			if (findStyle == null || style.equals(findStyle)) {
		    				System.out.println(sku + " #### " + beforeSkuVO.getIslive() + " *** " + beforeSkuVO.getSkuQtySold() + " ######## " + afterSkuVO.getIslive() + " *** " + afterSkuVO.getSkuQtySold() );
		    			}
		    		} else if (beforeSkuVO != null) {
		    			if (beforeSkuVO.getIslive() != 0) {
		    				beforeSkuCount++;
		    				beforeStyleVal += ((double)beforeSkuVO.getSkuQtySold()/(double)beforeSkuVO.getIslive());
		    			}
		    			if (findStyle == null || style.equals(findStyle)) {
		    				System.out.println(sku + " #### " + beforeSkuVO.getIslive() + " *** " + beforeSkuVO.getSkuQtySold() + " ######## " + afterSkuVO + " *** " + afterSkuVO );
		    			}
		    		} else if (afterSkuVO != null) {
		    			if (afterSkuVO.getIslive() != 0) {
		    				afterSkuCount++;
		    				afterStyleVal += ((double)afterSkuVO.getSkuQtySold()/(double)afterSkuVO.getIslive());
		    			}
		    			if (findStyle == null || style.equals(findStyle)) {
		    				System.out.println(sku + " #### " + beforeSkuVO + " *** " + beforeSkuVO + " ######## " + afterSkuVO.getIslive() + " *** " + afterSkuVO.getSkuQtySold() );
		    			}
		    		}
		    		
		    		
		    	}
		    	
		    	//
		    	if (findStyle == null || style.equals(findStyle)) {
		    		System.out.println(beforeStyleVal+ " *** " +beforePdpCount+ " *** " +beforeIsLiveMax + " ### " + afterStyleVal+ " *** " +afterPdpCount+ " *** " +afterIsLiveMax);
		    		
		    		double convDiff = (beforeStyleVal/(beforePdpCount/beforeIsLiveMax))-(afterStyleVal/(afterPdpCount/afterIsLiveMax));

			    	String ConvVal = "";
			    	if (convDiff < .05 && convDiff > -.05) {
			    		ConvVal = "equal";
			    	} else if (convDiff > .05) {
			    		ConvVal = "positive";
			    	} else if (convDiff < -.05) {
			    		ConvVal = "negative";
			    	} 

			    	System.out.println("Style Val : before : " + (beforeStyleVal/(beforePdpCount/beforeIsLiveMax)) + " after : " + (afterStyleVal/(afterPdpCount/afterIsLiveMax)) + "  analysis : " + ConvVal);
		    	
		    	}
		    	
		    	/*if (findStyle == null || style.equals(findStyle)) {
		    		System.out.println(beforeStyleVal+ " *** " +beforeSkuCount+ " *** " +beforePdpCount + " ### " + afterStyleVal+ " *** " +afterSkuCount+ " *** " +afterPdpCount);
		    		
		    		double convDiff = ((afterStyleVal/afterSkuCount)/afterPdpCount)-((beforeStyleVal/beforeSkuCount)/beforePdpCount);

			    	String ConvVal = "";
			    	if (convDiff < .05 && convDiff > -.05) {
			    		ConvVal = "equal";
			    	} else if (convDiff > .05) {
			    		ConvVal = "positive";
			    	} else if (convDiff < -.05) {
			    		ConvVal = "negative";
			    	} 

			    	System.out.println("Style Val : before : " + (beforeStyleVal/beforeSkuCount)/beforePdpCount + " after : " + (afterStyleVal/afterSkuCount)/afterPdpCount + "  analysis : " + ConvVal);
		    	
		    	}*/
	    	}
	    	
	    }
	    
	}


	private void writeToFileSku(Map<String, ListViews> beforeSkuMap) {

		String header = "StyleId,SkuId,islive,StyleQtySold,SkuQtySold,list,pdp,cart,articleType," +
				"brandType,brand,gender";

		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			//file = new File("/home/en-sorabhk/files/brokenSkuData.csv");
			file = new File("./brokenSkuData.csv");
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(header);

			for(Map.Entry<String, ListViews> entry : beforeSkuMap.entrySet()) {
				ListViews vo = entry.getValue();
				bufferedWriter.write("\n");
				bufferedWriter.write(vo.getStyle_id()+","+vo.getSku_id()+","+vo.getIslive()+","+vo.getStyleQtySold()
						+","+vo.getSkuQtySold()+","+vo.getList()+","+vo.getPdp()+","+vo.getCart()+","+vo.getArticle_type()
						+","+vo.getBrand_type()+","+vo.getBrand()+","+vo.getGender());

			}
		} catch (IOException e) {
			System.out.println("Exception raised while writing to report : " + e);
			e.printStackTrace();
			//logger.error("Exception raised while writing to report : " + e);
		} 
		finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception raised while closing writer objects : "+ e);
				//logger.error("Exception raised while closing writer objects : "+ e);
				e.printStackTrace();
			} 
		}

	}

	private Map<String, ListViews> trackbrokenData(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		//style_id, sku_id, is_live_on_portal

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();

			//int formattedDateInt = Integer.parseInt(formattedDate);
			//listViews.setDate(formattedDateInt);
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setSku_id(rs.getString("sku_id"));
			//listViews.setIslive(rs.getInt("is_live_on_portal"));
			listViews.setIslive(rs.getInt("isLive"));

			styleMap.put(listViews.getSku_id(), listViews);
		}
		return styleMap;
	}

	private Map<String, ListViews> trackskuData(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		//select sku_id, style_id, brand, brand_type, article_type, gender, quantity, order_created_date

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();

			//int formattedDateInt = Integer.parseInt(formattedDate);
			//listViews.setDate(formattedDateInt);
			listViews.setStyle_id(rs.getString("style_id"));
			listViews.setSku_id(rs.getString("sku_id"));
			//listViews.setCommercial_type(rs.getString("commercial_type"));
			listViews.setArticle_type(rs.getString("article_type"));
			listViews.setBrand_type(rs.getString("brand_type"));
			listViews.setBrand(rs.getString("brand"));
			listViews.setGender(rs.getString("gender"));
			//listViews.setList(rs.getLong("list"));
			//listViews.setPdp(rs.getLong("pdp"));
			//listViews.setCart(rs.getLong("cart"));
			//listViews.setStyleQtySold(rs.getLong("qtySold"));
			listViews.setSkuQtySold(rs.getLong("quantity"));
			styleMap.put(listViews.getSku_id(), listViews);
		}
		return styleMap;
	}

	/*private void writeToFile(Map<String, ListViews> beforeMap, Map<String, ListViews> afterMap, List<String> styleList) {

		String header = "style,beforeList,afterList,beforePdp,afterPdp,beforeCart,afterCart,beforeQtySold,afterQtySold" +
				",beforeConversion(pdp/list),afterConversion(pdp/list),conversionDiff(pdp/list)" +
				",beforeConversion(cart/pdp),afterConversion(cart/pdp),conversionDiff(cart/pdp)" +
				",beforeConversion(qtySold/pdp),afterConversion(qtySold/pdp),conversionDiff(qtySold/pdp)";

		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			file = new File("/home/en-sorabhk/files/unbroken_weekdays.csv");
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(header);

			for (String style : styleList) {
				ListViews before = beforeMap.get(style);
				ListViews after = afterMap.get(style);

				if(before != null && after != null) {
					bufferedWriter.write("\n");

					double bfrConvPdpList = (before.getList()!=0)?((double)before.getPdp()/(double)before.getList()):0;
					double aftConvPdpList = (after.getList()!=0)?((double)after.getPdp()/(double)after.getList()):0;

					double bfrConvCartPdp = (before.getPdp()!=0)?((double)before.getCart()/(double)before.getPdp()):0;
					double aftConvCartPdp = (after.getPdp()!=0)?((double)after.getCart()/(double)after.getPdp()):0;

					double bfrConvQtyPdp = (before.getPdp()!=0)?((double)before.getQtySold()/(double)before.getPdp()):0;
					double aftConvQtyPdp = (after.getPdp()!=0)?((double)after.getQtySold()/(double)after.getPdp()):0;

					bufferedWriter.write(style + "," + before.getList() + "," + after.getList() + 
							"," + before.getPdp() + "," + after.getPdp() + 
							"," + before.getCart() + "," + after.getCart() + 
							"," + before.getQtySold() + "," + after.getQtySold() +

							"," + bfrConvPdpList + "," + aftConvPdpList +
							"," + (bfrConvPdpList - aftConvPdpList) +

							"," + bfrConvCartPdp + "," + aftConvCartPdp +
							"," + (bfrConvCartPdp - aftConvCartPdp) +

							"," + bfrConvQtyPdp + "," + aftConvQtyPdp + 
							"," + (bfrConvQtyPdp - aftConvQtyPdp)
							);
				}
			}



			for(Map.Entry<String, StyleDetails> entry : dataMap.entrySet()) {
				StyleDetails val = entry.getValue();
				if (val!= null ) {
					bufferedWriter.write("\n");
					bufferedWriter.write(val.getStyleid() +", " + val.getCurrentTd()+", " + val.getNewTd()
							+", " + val.getMinDiscount()+", " + val.getMaxDiscount()+", " + val.getNorm_adj_bm_new_actual()+", " + val.getTotalscore()
							+", " + val.getDmdfinal()+", " + val.getArticlemrp()+", " + val.getArticletype()
							+", " + val.getGender()+", " + val.getDiscountfunding()
							+", " + val.getFundingpercentage()+", " + val.getDiscountlimit()
							+", " + val.getFrom_unixtime_expired_on()+", " + val.getGetcount()
							+", " + val.getGetamount()+", " + val.getBrand()+", " + val.getDiscountid()
							+", " + val.getBrandtype()+", " + val.getBusinessunit()+", " + val.getCurrentcommercialtype()
							+", " + val.getAverageage()
							+", " + val.getHealth()+", " + val.getNorm_adj_bm_breach()
							//+", " + val.getNewDemand()+", " + val.getRankingFactor()
							);
				} else {
					//logger.info("Data not found for style : " + val.getStyleid());
					System.out.println("Data not found for style : " + val.getStyleid());
				}
			}
		} catch (IOException e) {
			System.out.println("Exception raised while writing to report : " + e);
			e.printStackTrace();
			//logger.error("Exception raised while writing to report : " + e);
		} 
		finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception raised while closing writer objects : "+ e);
				//logger.error("Exception raised while closing writer objects : "+ e);
				e.printStackTrace();
			} 
		}

	}*/

	private static List<String> readFile(String fileName) {

		List<String> styleList = new ArrayList<String>();

		File file = new File(fileName);

		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String str;
		try {
			while ((str = in.readLine()) != null) {
				styleList.add(str);
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		return styleList;
	}

	private Map<String, ListViews> trackListViews(ResultSet rs, String formattedDate) throws Exception {

		Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

		ListViews listViews = null;
		while (rs.next()) {
			listViews = new ListViews();

			//int formattedDateInt = Integer.parseInt(formattedDate);
			//listViews.setDate(formattedDateInt);
			listViews.setStyle_id(rs.getString("style_id"));
			//listViews.setCommercial_type(rs.getString("commercial_type"));
			//listViews.setArticle_type(rs.getString("article_type"));
			//listViews.setBrand_type(rs.getString("brand_type"));
			//listViews.setBrand(rs.getString("brand"));
			//listViews.setGender(rs.getString("gender"));
			listViews.setList(rs.getLong("list"));
			listViews.setPdp(rs.getLong("pdp"));
			listViews.setCart(rs.getLong("cart"));
			listViews.setStyleQtySold(rs.getLong("qtySold"));
			styleMap.put(listViews.getStyle_id(), listViews);
		}
		return styleMap;
	}


	private String checkNull(String str) {
		if (str == null) {
			return "NULL";
		} 
		return str;
	}
}
