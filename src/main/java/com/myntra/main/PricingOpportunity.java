package com.myntra.main;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.myntra.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Session;
import com.myntra.sql.connector.CassandraConnector;
import com.myntra.sql.connector.RedShiftConnectionFactory;
import com.myntra.util.DmdFinalComparator;

public class PricingOpportunity {


    static String listViewsSql = "select sh.style_id, sum(list_views_count) as List, sum(pdp_views_count) as PDP, " +
            " sum(cart_views_count) as Cart, sum(qty_sold) as qtySold " +
            " from product.style_history sh " +
            " join product.style_details dp on sh.style_id=dp.style_id" +
            " where sh.entry_date between %s and %s " +
            " and dp.brand in %s " +
            " and dp.article_type = '%s' " +
            " and dp.gender = '%s' " +
            " group by sh.style_id ";

    static String skuSql = "select fci.sku_id, fci.style_id, fci.brand, fci.brand_type, fci.article_type, fci.gender, sum(fci.quantity) as quantity " +
            " from bidb.fact_core_item fci" +
            " join dim_product dp on fci.style_id=dp.style_id" +
            " where fci.order_created_date between %s and %s and (fci.is_shipped=1 or fci.is_realised =1) and fci.store_id = 1" +
            " and dp.brand in %s " +
            " and dp.article_type = '%s' " +
            " and dp.gender = '%s' " +
            " group by fci.sku_id, fci.style_id, fci.brand, fci.brand_type, fci.article_type, fci.gender ";

    static String brokenSql = "select fps.style_id, fps.sku_id, sum(fps.is_live_on_portal) as isLive from fact_product_snapshot fps" +
            " JOIN dim_product dp on fps.style_id=dp.style_id" +
            " where fps.date between %s and %s " +
            " and dp.brand in %s " +
            " and dp.article_type = '%s' " +
            " and dp.gender = '%s' " +
            " group by fps.style_id, fps.sku_id ";

    static String demandSql = "select ps.style_id, ps.last_7days_sales, ps.last_30days_sales from bidb.pricing_snapshot ps" +
            " JOIN dim_product dp on ps.style_id=dp.style_id" +
            " and dp.brand in %s " +
            " and dp.article_type = '%s' " +
            " and dp.gender = '%s' " +
            " limit 100000000";

    static String pricingSnapshotSql = "select ps.style_id, ps.article_type, ps.brand, ps.gender from pricing_snapshot ps where ps.is_live_on_portal=1 " +
            " and ps.brand in %s " +
            " and ps.article_type = '%s' " +
            " and ps.gender = '%s' " ;


    //	static Map<String, ListViews> styleInfoMap = new HashMap<String, ListViews>();
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    static SimpleDateFormat timeFormat = new SimpleDateFormat("HH");

    static long beforePdpTotal = 0;
    static long afterPdpTotal = 0;

    private static CassandraConnector cassandraConnector = new CassandraConnector();
    final static String[] cassandraIP = {"10.174.26.102","10.166.1.165","10.148.158.249"};

    private static Logger logger = LoggerFactory.getLogger(MainCompleteClassVisibility.class);

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws Exception{

        cassandraConnector.connect(cassandraIP, 9042);
        Session session = cassandraConnector.getSession();

        ArrayList<BAG> catList = new ArrayList<BAG>();

        if (args.length > 0){
            BAG category = new BAG(args[0], args[1], args[2]);
            catList.add(category);
        }
        else{
            catList = readBagCSV("./categories.csv");
        }

        final String lastWeekEnd = dateFormat.format(new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L));
        final String lastWeekBegin = dateFormat.format(new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L * 7L));
        final String olderWeekEnd = dateFormat.format(new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L * 8L));
        final String olderWeekBegin = dateFormat.format(new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L * 14L));

        final Map<String, ElasticityRackVo> rackMap = RackReader.readRackInfo(session);

//		Date date = new Date();
//		int currentDate = Integer.parseInt(dateFormat.format(date));
//		int currentTime = Integer.parseInt(timeFormat.format(date));
//		currentTime = (currentTime * 100) + 30;

        Thread threads[] = new Thread[catList.size()];
        for ( int i = 0; i < catList.size(); i++) {
            final BAG cat = catList.get(i);
            final int counter = i;
            threads[i] = new Thread() {
                String article_type;
                String gender;
                String brand;

                public void run() {
                    Connection connection;
                    Statement statement = null;
                    Connection monet_con;
                    Statement monet_st;
                    try {
                        connection = RedShiftConnectionFactory.getConnection();
                        statement = connection.createStatement();

                        monet_con = DriverManager.getConnection("jdbc:redshift://ds-datastore.clakoo3hd0vy.ap-southeast-1.redshift.amazonaws.com:5439/datascience", "readuser", "ReadPass101");
                        monet_st = monet_con.createStatement();

                        article_type = cat.getArticle_type();
                        gender = cat.getGender();
                        brand = cat.getBrand();

                        System.out.println(article_type + " " + gender);

                        Map<String, ListViews> beforeSkuMap = runQueries(statement, monet_st, olderWeekBegin, olderWeekEnd, "before", brand, article_type, gender);
                        System.out.println(beforeSkuMap.size());

                        Map<String, ListViews> afterSkuMap = runQueries(statement, monet_st, lastWeekBegin, lastWeekEnd, "after", brand, article_type, gender);
                        System.out.println(afterSkuMap.size());

                        String brandnew;
                        if (brand.equals("ModaRapido")) {
                            brandnew = "('Moda Rapido','Moda Rapido Disney','Moda Rapido Marvel','Moda Rapido Star Wars')";
                        }
                        else if (brand.equals("UnitedColorsofBenetton")){
                            brandnew = "('United Colors of Benetton')";
                        }
                        else {
                            brandnew = "('" + brand + "')";
                        }
                        System.out.println(String.format(pricingSnapshotSql, brandnew, article_type, gender));
                        ResultSet rsPricingSnapshot = statement.executeQuery(String.format(pricingSnapshotSql, brandnew, article_type, gender));
                        Map<String, PricingSnapshotDetails> styleList = trackPricingSnapshot(rsPricingSnapshot);
                        System.out.println("pricing snapshot list size: " + styleList.size());

                        Map<String, Double> demandMap = readDemand(statement, brandnew, article_type, gender);

                        analyse(beforeSkuMap, afterSkuMap, demandMap, rackMap, styleList, brand, article_type, gender);
                        statement.close();
                        connection.close();
                        System.out.println("Ending thread : " + counter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            threads[i].start();
        }
        for (int i = 0 ; i < threads.length; i++) {
            threads[i].join();
        }
        session.close();
        cassandraConnector.close();
    }


    private static Map<String, Double> readDemand(Statement statement, String brand, String article_type, String gender) throws SQLException {
        System.out.println(String.format(demandSql, brand, article_type, gender));
        ResultSet rs = statement.executeQuery(String.format(demandSql, brand, article_type, gender));

        Map<String, Double> demandMap = new HashMap<String, Double>();

        while (rs.next()) {
            String style = rs.getString("style_id");
            double last7dayssales = rs.getDouble("last_7days_sales");
            double last30dayssales = rs.getDouble("last_30days_sales");
            double dmdFinal = ((last7dayssales/7)+(last30dayssales/30))/2;

            demandMap.put(style, dmdFinal);
        }
        return demandMap;
    }


    public static Map<String, ListViews> runQueries(Statement statement, Statement statement2, String dateBegin, String dateEnd, String timeStatus, String brand, String article_type, String gender) throws Exception{
        // TODO Auto-generated method stub

        Map<String, ListViews> beforeSkuMap = null;

        if (statement != null) {
            if (brand.equals("ModaRapido")) {
                brand = "('Moda Rapido','Moda Rapido Disney','Moda Rapido Marvel','Moda Rapido Star Wars')";
            }
            else if (brand.equals("UnitedColorsofBenetton")){
                brand = "('United Colors of Benetton')";
            }
            else {
                brand = "('" + brand + "')";
            }
            System.out.println(String.format(listViewsSql, dateBegin, dateEnd, brand, article_type, gender));
            ResultSet rs1 = statement2.executeQuery(String.format(listViewsSql, dateBegin, dateEnd, brand, article_type, gender));
            Map<String, ListViews> beforeMap = trackListViews(rs1, timeStatus);

            System.out.println(String.format(skuSql, dateBegin, dateEnd, brand, article_type, gender));
            ResultSet rs2 = statement.executeQuery(String.format(skuSql, dateBegin, dateEnd, brand, article_type, gender));
            beforeSkuMap = trackskuData(rs2);

            System.out.println(String.format(brokenSql, dateBegin, dateEnd, brand, article_type, gender));
            ResultSet rs3 = statement.executeQuery(String.format(brokenSql, dateBegin, dateEnd, brand, article_type, gender));
            Map<String, ListViews> beforebrokenMap = trackbrokenData(rs3);


            for(Map.Entry<String, ListViews> entry : beforeSkuMap.entrySet()) {
                ListViews vo = entry.getValue();
                String styleId = vo.getStyle_id();
                String skuId = vo.getSku_id();

                ListViews styleDetails = beforeMap.get(styleId);
                if (styleDetails != null) {
                    vo.setList(styleDetails.getList());
                    vo.setPdp(styleDetails.getPdp());
                    vo.setCart(styleDetails.getCart());
                    vo.setStyleQtySold(styleDetails.getStyleQtySold());
                }

                ListViews skuDetails = beforebrokenMap.get(skuId);
                if (skuDetails != null) {
                    vo.setIslive(skuDetails.getIslive());
                }
                beforeSkuMap.put(skuId, vo);
            }
        }
        return beforeSkuMap;
    }

    private static Map<String, ListViews> trackbrokenData(ResultSet rs) throws Exception {

        Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

        ListViews listViews;
        while (rs.next()) {
            listViews = new ListViews();
            listViews.setStyle_id(rs.getString("style_id"));
            listViews.setSku_id(rs.getString("sku_id"));
            listViews.setIslive(rs.getInt("isLive"));
            styleMap.put(listViews.getSku_id(), listViews);
        }
        return styleMap;
    }

    private static Map<String, PricingSnapshotDetails> trackPricingSnapshot(ResultSet rs) throws Exception {
        Map<String, PricingSnapshotDetails> styleMap = new HashMap<String, PricingSnapshotDetails>();

        PricingSnapshotDetails psd;
        while(rs.next()) {
            psd = new PricingSnapshotDetails();
            psd.setStyleid(rs.getString("style_id"));
            psd.setArticle_type(rs.getString("article_type"));
            psd.setBrand(rs.getString("brand"));
            psd.setGender(rs.getString("gender"));
            styleMap.put(psd.getStyleid(), psd);
        }
        return styleMap;
    }

    private static Map<String, ListViews> trackskuData(ResultSet rs) throws Exception {

        Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

        ListViews listViews;
        while (rs.next()) {
            listViews = new ListViews();
            listViews.setStyle_id(rs.getString("style_id"));
            listViews.setSku_id(rs.getString("sku_id"));
            listViews.setArticle_type(rs.getString("article_type"));
            listViews.setBrand_type(rs.getString("brand_type"));
            listViews.setBrand(rs.getString("brand"));
            listViews.setGender(rs.getString("gender"));
            listViews.setSkuQtySold(rs.getLong("quantity"));
            styleMap.put(listViews.getSku_id(), listViews);
        }
        return styleMap;
    }

    private static Map<String, ListViews> trackListViews(ResultSet rs, String timeStatus) throws Exception {

        Map<String, ListViews> styleMap = new HashMap<String, ListViews>();

        ListViews listViews = null;
        while (rs.next()) {
            listViews = new ListViews();
            listViews.setStyle_id(rs.getString("style_id"));
            listViews.setList(rs.getLong("list"));
            listViews.setPdp(rs.getLong("pdp"));


            if (timeStatus.equals("before")) {
                beforePdpTotal += rs.getLong("pdp");
            } else if (timeStatus.equals("after")) {
                afterPdpTotal += rs.getLong("pdp");
            }

            listViews.setCart(rs.getLong("cart"));
            listViews.setStyleQtySold(rs.getLong("qtySold"));
            styleMap.put(listViews.getStyle_id(), listViews);
        }
        return styleMap;
    }

    private static void analyse(Map<String, ListViews> beforeSkuMap2, Map<String, ListViews> afterSkuMap2, Map<String, Double> demandMap, Map<String, ElasticityRackVo> rackMap, Map<String, PricingSnapshotDetails> stylePsMap, String brand, String article_type, String gender) {

        //style level sku data map
        Map<String, Map<String,ListViews>> beforeMap = new HashMap<String, Map<String,ListViews>>();


        for (Map.Entry<String, ListViews> entry : beforeSkuMap2.entrySet()) {
            ListViews vo = entry.getValue();
            if (beforeMap.containsKey(vo.getStyle_id())) {
                Map<String,ListViews> map = beforeMap.get(vo.getStyle_id());
                map.put(vo.getSku_id(), vo);
                beforeMap.put(vo.getStyle_id(), map);
            } else {
                Map<String,ListViews> map = new HashMap<String, ListViews>();
                map.put(vo.getSku_id(), vo);
                beforeMap.put(vo.getStyle_id(), map);
            }
        }

        //style level sku data map
        Map<String, Map<String,ListViews>> afterMap = new HashMap<String, Map<String,ListViews>>();

        for (Map.Entry<String, ListViews> entry : afterSkuMap2.entrySet()) {
            ListViews vo = entry.getValue();
            if (afterMap.containsKey(vo.getStyle_id())) {
                Map<String,ListViews> map = afterMap.get(vo.getStyle_id());
                map.put(vo.getSku_id(), vo);
                afterMap.put(vo.getStyle_id(), map);
            } else {
                Map<String,ListViews> map = new HashMap<String, ListViews>();
                map.put(vo.getSku_id(), vo);
                afterMap.put(vo.getStyle_id(), map);
            }
        }

        System.out.println("before map size:" + beforeMap.size());
        System.out.println("after map size:" + afterMap.size());

        Set s1 = new HashSet(beforeMap.keySet());
        Set s2 = new HashSet(afterMap.keySet());
        s1.retainAll(s2);


        int equalCount = 0;
        int positiveCount = 0;
        int negativeCount = 0;

        List<String> equalList = new ArrayList<String>();
        List<String> positiveList = new ArrayList<String>();
        List<String> negativeList = new ArrayList<String>();

        int pdpIncQtyInc = 0;
        int pdpDecQtyInc = 0;
        int pdpIncQtyDec = 0;
        int pdpDecQtyDec = 0;

        List<String> pdpIncQtyIncList = new ArrayList<String>();
        List<String> pdpDecQtyIncList = new ArrayList<String>();
        List<String> pdpIncQtyDecList = new ArrayList<String>();
        List<String> pdpDecQtyDecList = new ArrayList<String>();

        for (Map.Entry<String, Map<String,ListViews>> entry : afterMap.entrySet()) {

            String style = entry.getKey();

            //if (style.equals("1311404")) {

            Map<String, ListViews> afterSkuMap = entry.getValue();
            Map<String, ListViews> beforeSkuMap = beforeMap.get(style);

            //if (afterSkuMap != null && beforeSkuMap != null && afterSkuMap.size() < beforeSkuMap.size()) {

            System.out.println("**********************************************");

            int totalQtySoldBefore = 0;
            int totalQtySoldAfter = 0;

            long beforePdpCount = 0;
            long afterPdpCount = 0;

            int beforeIsLiveMax = 0;
            int afterIsLiveMax = 0;

            if (beforeSkuMap!=null) {

                Set<String> skuSet = new HashSet<String>();

                for (Map.Entry<String, ListViews> skuEntry : beforeSkuMap.entrySet()) {
                    ListViews val = skuEntry.getValue();
                    beforePdpCount = val.getPdp();
                    totalQtySoldBefore += val.getSkuQtySold();
                    if (val.getIslive() > beforeIsLiveMax) {
                        beforeIsLiveMax = val.getIslive();
                    }

                    skuSet.add(skuEntry.getKey());
                }

                for (Map.Entry<String, ListViews> skuEntry : afterSkuMap.entrySet()) {
                    ListViews val = skuEntry.getValue();
                    afterPdpCount = val.getPdp();
                    totalQtySoldAfter += val.getSkuQtySold();
                    if (val.getIslive() > afterIsLiveMax) {
                        afterIsLiveMax = val.getIslive();
                    }

                    skuSet.add(skuEntry.getKey());
                }

                //System.out.println("  ********************************************  ");
                //System.out.println(style + "  " + totalQtySoldBefore + "  " + totalQtySoldAfter);


                int beforeSkuCount = 0;
                double beforeStyleVal = 0;

                int afterSkuCount = 0;
                double afterStyleVal = 0;

                for (String sku : skuSet) {

                    ListViews afterSkuVO = afterSkuMap.get(sku);
                    ListViews beforeSkuVO = beforeSkuMap.get(sku);

                    if (beforeSkuVO != null && afterSkuVO != null) {
                        if (beforeSkuVO.getIslive() != 0) {
                            beforeSkuCount++;
                            beforeStyleVal += ((double)beforeSkuVO.getSkuQtySold()/(double)beforeSkuVO.getIslive());
                        }
                        if (afterSkuVO.getIslive() != 0) {
                            afterSkuCount++;
                            afterStyleVal += ((double)afterSkuVO.getSkuQtySold()/(double)afterSkuVO.getIslive());
                        }
                        System.out.println(sku + " #### " + beforeSkuVO.getIslive() + " *** " + beforeSkuVO.getSkuQtySold() + " ######## " + afterSkuVO.getIslive() + " *** " + afterSkuVO.getSkuQtySold() );
                    } else if (beforeSkuVO != null) {
                        if (beforeSkuVO.getIslive() != 0) {
                            beforeSkuCount++;
                            beforeStyleVal += ((double)beforeSkuVO.getSkuQtySold()/(double)beforeSkuVO.getIslive());
                        }
                        System.out.println(sku + " #### " + beforeSkuVO.getIslive() + " *** " + beforeSkuVO.getSkuQtySold() + " ######## " + afterSkuVO + " *** " + afterSkuVO );
                        //System.out.println(sku + " *** " + beforeSkuVO.getSkuQtySold() + " *** " + afterSkuVO );
                    } else if (afterSkuVO != null) {
                        if (afterSkuVO.getIslive() != 0) {
                            afterSkuCount++;
                            afterStyleVal += ((double)afterSkuVO.getSkuQtySold()/(double)afterSkuVO.getIslive());
                        }
                        System.out.println(sku + " #### " + beforeSkuVO + " *** " + beforeSkuVO + " ######## " + afterSkuVO.getIslive() + " *** " + afterSkuVO.getSkuQtySold() );
                        //System.out.println(sku + " *** " + beforeSkuVO + " *** " + afterSkuVO.getSkuQtySold() );
                    }
                }

                System.out.println(beforeSkuCount + " $$$ " + beforePdpCount + " $$$ " + beforeIsLiveMax);
                System.out.println(afterSkuCount + " $$$ " + afterPdpCount + " $$$ " + afterIsLiveMax);

                //System.out.println("Style Val : before : " + (beforeStyleVal/beforeSkuCount)/beforePdpCount + " after : " + (afterStyleVal/afterSkuCount)/afterPdpCount);
                if (beforeIsLiveMax != 0 && afterIsLiveMax != 0) {
                    //System.out.println(" *** " + beforeStyleVal +" *** "+beforePdpCount+" *** "+beforeIsLiveMax+" *** "+afterStyleVal+" *** "+afterPdpCount+" *** "+afterIsLiveMax);


                    //for normalized conversion
                    double beforeConversion = ((beforeStyleVal*(skuSet.size()/beforeSkuCount))/(beforePdpCount/beforeIsLiveMax));
                    double afterConversion = ((afterStyleVal*(skuSet.size()/afterSkuCount))/(afterPdpCount/afterIsLiveMax));


                    //for actual conversion
					/*double beforeConversion = ((beforeStyleVal)/(beforePdpCount/beforeIsLiveMax));
			    	double afterConversion = ((afterStyleVal)/(afterPdpCount/afterIsLiveMax));
					 */

                    double convChange = (100/beforeConversion)*afterConversion;

                    String ConvVal = "";
                    if (convChange < 120 && convChange > 80) {
                        ConvVal = "equal";
                        equalCount++;
                        equalList.add(style);
                    } else if (convChange > 120) {
                        ConvVal = "positive";
                        positiveCount++;
                        positiveList.add(style);
                    } else if (convChange < 80) {
                        ConvVal = "negative";
                        negativeCount++;
                        negativeList.add(style);
                    }


                    System.out.println(style+" ### "+convChange+" ### "+beforeConversion+" *** "+afterConversion+" ### "+beforePdpTotal+" *** "+afterPdpTotal+" ### "+beforePdpCount+" *** "+afterPdpCount+" ### "+totalQtySoldBefore+" *** "+totalQtySoldAfter +" *** "+ConvVal);

                    double beforePdpContribution = (double)beforePdpCount/((double)beforePdpTotal/10000);
                    double afterPdpContribution = (double)afterPdpCount/((double)afterPdpTotal/10000);

                    double pdpContributionChange = (100/beforePdpContribution)*afterPdpContribution;

                    if (pdpContributionChange > 120 && ConvVal.equals("positive")) {
                        pdpIncQtyInc++;
                        pdpIncQtyIncList.add(style);
                    }
                    if (pdpContributionChange > 120 && ConvVal.equals("negative")) {
                        pdpIncQtyDec++;
                        pdpIncQtyDecList.add(style);
                    }
                    if (pdpContributionChange < 80 && ConvVal.equals("positive")) {
                        pdpDecQtyInc++;
                        pdpDecQtyIncList.add(style);
                    }
                    if (pdpContributionChange < 80 && ConvVal.equals("negative")) {
                        pdpDecQtyDec++;
                        pdpDecQtyDecList.add(style);
                    }

                }
                ////
            }

            //} //if(style)  or //if (afterSkuMap.size() < beforeSkuMap.size()) {

        }

        System.out.println("**********************************************");

        System.out.println("Equal : " + equalCount +"  positive : "+ positiveCount + " negative : " + negativeCount);
        System.out.println(pdpIncQtyInc + "    " + pdpIncQtyDec + "    " + pdpDecQtyInc + "   " + pdpDecQtyDec);


        System.out.println("pdpIncQtyIncList  :  "+ pdpIncQtyIncList);
        System.out.println("pdpDecQtyIncList  :  "+ pdpDecQtyIncList);
        System.out.println("pdpIncQtyDecList  :  "+ pdpIncQtyDecList);
        System.out.println("pdpDecQtyDecList  :  "+ pdpDecQtyDecList);


        pdpIncQtyIncList = sortForVisibility(pdpIncQtyIncList, demandMap, rackMap);
        pdpDecQtyIncList = sortForVisibility(pdpDecQtyIncList, demandMap, rackMap);
        pdpIncQtyDecList = sortForVisibility(pdpIncQtyDecList, demandMap, rackMap);
        pdpDecQtyDecList = sortForVisibility(pdpDecQtyDecList, demandMap, rackMap);


        System.out.println("sorted pdpIncQtyIncList  :  "+ pdpIncQtyIncList);
        System.out.println("sorted pdpDecQtyIncList  :  "+ pdpDecQtyIncList);
        System.out.println("sorted pdpIncQtyDecList  :  "+ pdpIncQtyDecList);
        System.out.println("sorted pdpDecQtyDecList  :  "+ pdpDecQtyDecList);


		/* System.out.println("equalList : " + equalList);
	    System.out.println("positiveList : " + positiveList);
	    System.out.println("negativeList : " + negativeList);

	    System.out.println("#################################################");

	    for (String sty : positiveList) {
	    	if (!pdpIncQtyIncStr.contains(sty) && !pdpDecQtyIncStr.contains(sty)) {
	    		System.out.println(sty);
	    	}
	    }*/

        //ComputedSnapshotReader.readComputedSnapshot(session, currentDate, currentTime)

//	    categoryLevelAnalysis(pdpIncQtyIncList, rackMap, "pdpIncQtyIncList");
//	    categoryLevelAnalysis(pdpDecQtyIncList, rackMap, "pdpDecQtyIncList");
//	    categoryLevelAnalysis(pdpIncQtyDecList, rackMap, "pdpIncQtyDecList");
//	    categoryLevelAnalysis(pdpDecQtyDecList, rackMap, "pdpDecQtyDecList");

        try {
            for (String styleid: pdpDecQtyIncList){
                stylePsMap.remove(styleid);
            }
        }
        catch (NullPointerException ne) {
            System.out.println("pdpDecQtyIncList list empty.");
        }

        System.out.println("final size" + stylePsMap.size());
        List<String> finalStyleList = new ArrayList<String>(stylePsMap.keySet());
        System.out.println(finalStyleList.toString());
        categoryLevelAnalysis(finalStyleList, rackMap, stylePsMap, brand.toLowerCase() + article_type.toLowerCase() + gender.toLowerCase());
    }


    private static void categoryLevelAnalysis(List<String> list, Map<String, ElasticityRackVo> rackMap, Map<String, PricingSnapshotDetails> stylePsMap, String fileName) {

        File file = null;
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            file = new File("./"+fileName+".csv");
            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("styleid,articleType,gender,brand,elasticity,rack,collection,cascElasticity_CD,cascElasticity_TD,cascElasticity_unif,cascVisibility_Elasticity");
            for (String style : list) {
                bufferedWriter.write("\n");
//				System.out.println(style);
                PricingSnapshotDetails stylePsInfo = stylePsMap.get(style);
//				ListViews styleInfoVO = styleInfoMap.get(style);
//				System.out.println(styleInfoVO.toString());
                String brand = stylePsInfo.getBrand(); //styleInfoVO.getBrand();
                String articleType = stylePsInfo.getArticle_type(); //styleInfoVO.getArticle_type();
                String gender = stylePsInfo.getGender(); //styleInfoVO.getGender();

                ElasticityRackVo vo = rackMap.get(style);

                if (vo != null) {
                    //bufferedWriter.write(style+","+vo.getArticleType()+","+vo.getGender()+","+vo.getElasticity()+","+vo.getRack()+","+vo.getCollection()+","+vo.getCascElasticity_CD()+","+vo.getCascElasticity_TD()+","+vo.getCascElasticity_unif()+","+vo.getCascVisibility_Elasticity());
                    bufferedWriter.write(style+","+articleType+","+gender+","+brand+","+vo.getElasticity()+","+vo.getRack()+","+vo.getCollection()+","+vo.getCascElasticity_CD()+","+vo.getCascElasticity_TD()+","+vo.getCascElasticity_unif()+","+vo.getCascVisibility_Elasticity());
                } else {
                    //bufferedWriter.write(style+","+vo);
                    bufferedWriter.write(style+","+articleType+","+gender+","+brand+","+vo);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                bufferedWriter.close();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

	/*private static List<String> sortForVisibility(List<String> styleList, Map<String, Double> demandMap, Map<String, ElasticityRackVo> rackMap) {

		Map<Integer, List<StyleDemand>> elasticityStyleMap = new HashMap<Integer, List<StyleDemand>>();

		for(String style : styleList) {

			Double demand = demandMap.get(style);

			ElasticityRackVo rackVo = rackMap.get(style);

			if (rackVo != null) {
				int elasticity = rackVo.getElasticity();
				List<StyleDemand> list = null;
				if (elasticityStyleMap.containsKey(elasticity)) {
					list = elasticityStyleMap.get(elasticity);
				} else {
					list = new ArrayList<StyleDemand>();
				}
				StyleDemand styleDemand = new StyleDemand();
				styleDemand.setDemand(demand);
				styleDemand.setStyle_id(style);
				list.add(styleDemand);
				elasticityStyleMap.put(elasticity, list);
			} else {
				List<StyleDemand> list = null;
				if (elasticityStyleMap.containsKey(0)) {
					list = elasticityStyleMap.get(0);
				} else {
					list = new ArrayList<StyleDemand>();
				}

				StyleDemand styleDemand = new StyleDemand();
				styleDemand.setDemand(demand);
				styleDemand.setStyle_id(style);
				list.add(styleDemand);
				elasticityStyleMap.put(0, list);
			}
		}

		List<String> sortedList = new ArrayList<String>();

		int elasticityRank[] = {4,3,0,2,1};

		for (int elasticity : elasticityRank) {

			List<StyleDemand> list = elasticityStyleMap.get(elasticity);

			Collections.sort(list, new DmdFinalComparator());
			Collections.reverse(list);

			for(StyleDemand vo : list) {
				sortedList.add(vo.getStyle_id());
			}
		}

		return sortedList;
	}*/

    private static List<String> sortForVisibility(List<String> styleList, Map<String, Double> demandMap, Map<String, ElasticityRackVo> rackMap) {

        List<StyleDemand> visibilityDemandList = new ArrayList<StyleDemand>();

        for(String style : styleList) {
            double demand = (demandMap.get(style)!=null?demandMap.get(style):0);
            ElasticityRackVo rackVo = rackMap.get(style);
            double cascVisibilityElasticity = 1;
            if (rackVo != null) {
                try {
                    cascVisibilityElasticity = Double.parseDouble(rackVo.getCascVisibility_Elasticity());
                } catch (Exception e){
                }
            }
            StyleDemand styleDemand = new StyleDemand();
            styleDemand.setDemand(demand);
            styleDemand.setStyle_id(style);
            styleDemand.setCascVisibility_Elasticity(cascVisibilityElasticity);
            styleDemand.setVisibilityDemand(demand * cascVisibilityElasticity);
            visibilityDemandList.add(styleDemand);
        }

        List<String> sortedList = new ArrayList<String>();

        Collections.sort(visibilityDemandList, new DmdFinalComparator());
        Collections.reverse(visibilityDemandList);

        for(StyleDemand vo : visibilityDemandList) {
            //System.out.println(vo.getVisibilityDemand());
            sortedList.add(vo.getStyle_id());
        }

        return sortedList;
    }

    public static ArrayList<BAG> readBagCSV(String filename){
        BufferedReader br = null;
        ArrayList<BAG> catList = new ArrayList<BAG>();
        try {
            br = new BufferedReader(new FileReader(new File(filename)));
            String line;
            line = br.readLine();
            while ((line=br.readLine()) != null){
                String []strings = line.split(",");
                BAG bag = new BAG(strings[0], strings[1], strings[2]);
                catList.add(bag);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return catList;
    }


}
