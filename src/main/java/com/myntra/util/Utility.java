package com.myntra.util;

public class Utility {

	public static double checkForNan(double num) {

		if (((Double)num).isNaN() || ((Double)num).isInfinite()) {
			return 0;
		}
		return num;
	}

	public static double convertToDouble(String num) {

		if (num != null && !num.trim().equals("")) {
			try
			{
				return Double.parseDouble(num);
			}
			catch(NumberFormatException e)
			{
				System.out.println("error while parsing string "+ num + " to double ");
				return 0;
			}
		}
		return 0;
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

	public static boolean isDouble(String num) {

		if (num != null && !num.trim().equals("")) {
			try
			{
				Double.parseDouble(num);
				return true;
			}
			catch(NumberFormatException e)
			{
				return false;
			}
		}
		return false;
	}

	public static boolean isInt(String num) {

		try
		{
			Integer.parseInt(num);
		}
		catch(NumberFormatException e)
		{
			return false;
		}
		return true;

	}

}
