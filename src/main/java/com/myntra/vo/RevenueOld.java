package com.myntra.vo;

public class RevenueOld {
	
	private double revTot = 0;
	private double tdTot = 0;
	private double cdTot = 0;
	private double rgmTot = 0;
	
	private double caShoeM_rev = 0;
	private double caShoeM_td = 0;
	private double caShoeM_cd = 0;
	private double caShoeM_rgm = 0;
	
	private double tshirtsM_rev = 0;
	private double tshirtsM_td = 0;
	private double tshirtsM_cd = 0;
	private double tshirtsM_rgm = 0;
	
	private double jeansM_rev = 0;
	private double jeansM_td = 0;
	private double jeansM_cd = 0;
	private double jeansM_rgm = 0;
	
	private double caShoeM_rev_cPerc;
	private double caShoeM_td_cPerc;
	private double caShoeM_cd_cPerc;
	private double caShoeM_rgm_cPerc;
	
	private double tshirtsM_rev_cPerc;
	private double tshirtsM_td_cPerc;
	private double tshirtsM_cd_cPerc;
	private double tshirtsM_rgm_cPerc;
	
	private double JeansM_rev_cPerc;
	private double JeansM_td_cPerc;
	private double JeansM_cd_cPerc;
	private double JeansM_rgm_cPerc;
	
	public double getRevTot() {
		return revTot;
	}
	public void setRevTot(double revTot) {
		this.revTot = revTot;
	}
	public double getTdTot() {
		return tdTot;
	}
	public void setTdTot(double tdTot) {
		this.tdTot = tdTot;
	}
	public double getRgmTot() {
		return rgmTot;
	}
	public void setRgmTot(double rgmTot) {
		this.rgmTot = rgmTot;
	}
	public double getCaShoeM_rev() {
		return caShoeM_rev;
	}
	public void setCaShoeM_rev(double caShoeM_rev) {
		this.caShoeM_rev = caShoeM_rev;
	}
	public double getCaShoeM_td() {
		return caShoeM_td;
	}
	public void setCaShoeM_td(double caShoeM_td) {
		this.caShoeM_td = caShoeM_td;
	}
	public double getCaShoeM_rgm() {
		return caShoeM_rgm;
	}
	public void setCaShoeM_rgm(double caShoeM_rgm) {
		this.caShoeM_rgm = caShoeM_rgm;
	}
	public double getTshirtsM_rev() {
		return tshirtsM_rev;
	}
	public void setTshirtsM_rev(double tshirtsM_rev) {
		this.tshirtsM_rev = tshirtsM_rev;
	}
	public double getTshirtsM_td() {
		return tshirtsM_td;
	}
	public void setTshirtsM_td(double tshirtsM_td) {
		this.tshirtsM_td = tshirtsM_td;
	}
	public double getTshirtsM_rgm() {
		return tshirtsM_rgm;
	}
	public void setTshirtsM_rgm(double tshirtsM_rgm) {
		this.tshirtsM_rgm = tshirtsM_rgm;
	}
	public double getJeansM_rev() {
		return jeansM_rev;
	}
	public void setJeansM_rev(double jeansM_rev) {
		this.jeansM_rev = jeansM_rev;
	}
	public double getJeansM_td() {
		return jeansM_td;
	}
	public void setJeansM_td(double jeansM_td) {
		this.jeansM_td = jeansM_td;
	}
	public double getJeansM_rgm() {
		return jeansM_rgm;
	}
	public void setJeansM_rgm(double jeansM_rgm) {
		this.jeansM_rgm = jeansM_rgm;
	}
	public double getCaShoeM_rev_cPerc() {
		return caShoeM_rev_cPerc;
	}
	public void setCaShoeM_rev_cPerc(double caShoeM_rev_cPerc) {
		this.caShoeM_rev_cPerc = caShoeM_rev_cPerc;
	}
	public double getCaShoeM_td_cPerc() {
		return caShoeM_td_cPerc;
	}
	public void setCaShoeM_td_cPerc(double caShoeM_td_cPerc) {
		this.caShoeM_td_cPerc = caShoeM_td_cPerc;
	}
	public double getCaShoeM_rgm_cPerc() {
		return caShoeM_rgm_cPerc;
	}
	public void setCaShoeM_rgm_cPerc(double caShoeM_rgm_cPerc) {
		this.caShoeM_rgm_cPerc = caShoeM_rgm_cPerc;
	}
	public double getTshirtsM_rev_cPerc() {
		return tshirtsM_rev_cPerc;
	}
	public void setTshirtsM_rev_cPerc(double tshirtsM_rev_cPerc) {
		this.tshirtsM_rev_cPerc = tshirtsM_rev_cPerc;
	}
	public double getTshirtsM_td_cPerc() {
		return tshirtsM_td_cPerc;
	}
	public void setTshirtsM_td_cPerc(double tshirtsM_td_cPerc) {
		this.tshirtsM_td_cPerc = tshirtsM_td_cPerc;
	}
	public double getTshirtsM_rgm_cPerc() {
		return tshirtsM_rgm_cPerc;
	}
	public void setTshirtsM_rgm_cPerc(double tshirtsM_rgm_cPerc) {
		this.tshirtsM_rgm_cPerc = tshirtsM_rgm_cPerc;
	}
	public double getJeansM_rev_cPerc() {
		return JeansM_rev_cPerc;
	}
	public void setJeansM_rev_cPerc(double jeansM_rev_cPerc) {
		JeansM_rev_cPerc = jeansM_rev_cPerc;
	}
	public double getJeansM_td_cPerc() {
		return JeansM_td_cPerc;
	}
	public void setJeansM_td_cPerc(double jeansM_td_cPerc) {
		JeansM_td_cPerc = jeansM_td_cPerc;
	}
	public double getJeansM_rgm_cPerc() {
		return JeansM_rgm_cPerc;
	}
	public void setJeansM_rgm_cPerc(double jeansM_rgm_cPerc) {
		JeansM_rgm_cPerc = jeansM_rgm_cPerc;
	}
	
	public double getCdTot() {
		return cdTot;
	}
	public void setCdTot(double cdTot) {
		this.cdTot = cdTot;
	}
	public double getCaShoeM_cd() {
		return caShoeM_cd;
	}
	public void setCaShoeM_cd(double caShoeM_cd) {
		this.caShoeM_cd = caShoeM_cd;
	}
	public double getTshirtsM_cd() {
		return tshirtsM_cd;
	}
	public void setTshirtsM_cd(double tshirtsM_cd) {
		this.tshirtsM_cd = tshirtsM_cd;
	}
	public double getJeansM_cd() {
		return jeansM_cd;
	}
	public void setJeansM_cd(double jeansM_cd) {
		this.jeansM_cd = jeansM_cd;
	}
	public double getCaShoeM_cd_cPerc() {
		return caShoeM_cd_cPerc;
	}
	public void setCaShoeM_cd_cPerc(double caShoeM_cd_cPerc) {
		this.caShoeM_cd_cPerc = caShoeM_cd_cPerc;
	}
	public double getTshirtsM_cd_cPerc() {
		return tshirtsM_cd_cPerc;
	}
	public void setTshirtsM_cd_cPerc(double tshirtsM_cd_cPerc) {
		this.tshirtsM_cd_cPerc = tshirtsM_cd_cPerc;
	}
	public double getJeansM_cd_cPerc() {
		return JeansM_cd_cPerc;
	}
	public void setJeansM_cd_cPerc(double jeansM_cd_cPerc) {
		JeansM_cd_cPerc = jeansM_cd_cPerc;
	}
	public String toString() {
		
		return "revTot = "+revTot + "\ntdTot = " +tdTot+ "\ncdTot = " +cdTot+"\nrgmTot = " + rgmTot + 
				"\ncaShoeM_rev = " + caShoeM_rev + "\ncaShoeM_td = " +caShoeM_td+ "\ncaShoeM_cd = " +caShoeM_cd + 
				"\ncaShoeM_rgm = " +caShoeM_rgm + "\ntshirtsM_rev = " +tshirtsM_rev + 
				"\ntshirtsM_td = " +tshirtsM_td +"\ntshirtsM_cd = " +tshirtsM_cd + "\ntshirtsM_rgm = " +tshirtsM_rgm + 
				"\njeansM_rev = " + jeansM_rev + "\njeansM_td = " + jeansM_td + "\njeansM_cd = " + jeansM_cd + 
				"\njeansM_rgm = " + jeansM_rgm + "\ncaShoeM_rev_cPerc = " + caShoeM_rev_cPerc +
				"\ncaShoeM_td_cPerc = " + caShoeM_td_cPerc + "\ncaShoeM_cd_cPerc = " + caShoeM_cd_cPerc + 
				"\ncaShoeM_rgm_cPerc = " + caShoeM_rgm_cPerc + "\ntshirtsM_rev_cPerc = " + tshirtsM_rev_cPerc + 
				"\ntshirtsM_td_cPerc = " + tshirtsM_td_cPerc + "\ntshirtsM_cd_cPerc = " + tshirtsM_cd_cPerc +
				"\ntshirtsM_rgm_cPerc = " + tshirtsM_rgm_cPerc+ "\nJeansM_rev_cPerc = " + JeansM_rev_cPerc +
				"\nJeansM_td_cPerc = " + JeansM_td_cPerc + "\nJeansM_cd_cPerc = " + JeansM_cd_cPerc + 
				"\nJeansM_rgm_cPerc = " + JeansM_rgm_cPerc;
		
	}
	
}
