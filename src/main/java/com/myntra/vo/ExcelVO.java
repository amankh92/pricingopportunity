package com.myntra.vo;

public class ExcelVO {
	
	String style;
	long beforeList;
	long afterList;
	long beforePdp;
	long afterPdp;
	long beforeCart;
	long afterCart;
	long beforeQtySold;
	long afterQtySold;
	double beforeConversionPdpList;
	double afterConversionPdpList;
	double conversionDiffPdpList;
	double beforeConversionCartPdp;
	double afterConversionCartPdp;
	double conversionDiffCartPdp;
	double beforeConversionQtySoldPdp;
	double afterConversionQtySoldPdp;
	double conversionDiffQtySoldPdp;
	
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public long getBeforeList() {
		return beforeList;
	}
	public void setBeforeList(long beforeList) {
		this.beforeList = beforeList;
	}
	public long getAfterList() {
		return afterList;
	}
	public void setAfterList(long afterList) {
		this.afterList = afterList;
	}
	public long getBeforePdp() {
		return beforePdp;
	}
	public void setBeforePdp(long beforePdp) {
		this.beforePdp = beforePdp;
	}
	public long getAfterPdp() {
		return afterPdp;
	}
	public void setAfterPdp(long afterPdp) {
		this.afterPdp = afterPdp;
	}
	public long getBeforeCart() {
		return beforeCart;
	}
	public void setBeforeCart(long beforeCart) {
		this.beforeCart = beforeCart;
	}
	public long getAfterCart() {
		return afterCart;
	}
	public void setAfterCart(long afterCart) {
		this.afterCart = afterCart;
	}
	public long getBeforeQtySold() {
		return beforeQtySold;
	}
	public void setBeforeQtySold(long beforeQtySold) {
		this.beforeQtySold = beforeQtySold;
	}
	public long getAfterQtySold() {
		return afterQtySold;
	}
	public void setAfterQtySold(long afterQtySold) {
		this.afterQtySold = afterQtySold;
	}
	public double getBeforeConversionPdpList() {
		return beforeConversionPdpList;
	}
	public void setBeforeConversionPdpList(double beforeConversionPdpList) {
		this.beforeConversionPdpList = beforeConversionPdpList;
	}
	public double getAfterConversionPdpList() {
		return afterConversionPdpList;
	}
	public void setAfterConversionPdpList(double afterConversionPdpList) {
		this.afterConversionPdpList = afterConversionPdpList;
	}
	public double getConversionDiffPdpList() {
		return conversionDiffPdpList;
	}
	public void setConversionDiffPdpList(double conversionDiffPdpList) {
		this.conversionDiffPdpList = conversionDiffPdpList;
	}
	public double getBeforeConversionCartPdp() {
		return beforeConversionCartPdp;
	}
	public void setBeforeConversionCartPdp(double beforeConversionCartPdp) {
		this.beforeConversionCartPdp = beforeConversionCartPdp;
	}
	public double getAfterConversionCartPdp() {
		return afterConversionCartPdp;
	}
	public void setAfterConversionCartPdp(double afterConversionCartPdp) {
		this.afterConversionCartPdp = afterConversionCartPdp;
	}
	public double getConversionDiffCartPdp() {
		return conversionDiffCartPdp;
	}
	public void setConversionDiffCartPdp(double conversionDiffCartPdp) {
		this.conversionDiffCartPdp = conversionDiffCartPdp;
	}
	public double getBeforeConversionQtySoldPdp() {
		return beforeConversionQtySoldPdp;
	}
	public void setBeforeConversionQtySoldPdp(double beforeConversionQtySoldPdp) {
		this.beforeConversionQtySoldPdp = beforeConversionQtySoldPdp;
	}
	public double getAfterConversionQtySoldPdp() {
		return afterConversionQtySoldPdp;
	}
	public void setAfterConversionQtySoldPdp(double afterConversionQtySoldPdp) {
		this.afterConversionQtySoldPdp = afterConversionQtySoldPdp;
	}
	public double getConversionDiffQtySoldPdp() {
		return conversionDiffQtySoldPdp;
	}
	public void setConversionDiffQtySoldPdp(double conversionDiffQtySoldPdp) {
		this.conversionDiffQtySoldPdp = conversionDiffQtySoldPdp;
	}
	
}
