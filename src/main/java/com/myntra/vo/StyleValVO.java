package com.myntra.vo;

public class StyleValVO {
	
	double qtySoldNormalised;
	long pdpTotal;
	double pdpPerDay;
	int skuCount;
	String article_type = null;
	String brand_type = null;
	String brand = null;
	String gender = null;
	
	public double getQtySoldNormalised() {
		return qtySoldNormalised;
	}
	public void setQtySoldNormalised(double qtySoldNormalised) {
		this.qtySoldNormalised = qtySoldNormalised;
	}
	public long getPdpTotal() {
		return pdpTotal;
	}
	public void setPdpTotal(long pdpTotal) {
		this.pdpTotal = pdpTotal;
	}
	public double getPdpPerDay() {
		return pdpPerDay;
	}
	public void setPdpPerDay(double pdpPerDay) {
		this.pdpPerDay = pdpPerDay;
	}
	public int getSkuCount() {
		return skuCount;
	}
	public void setSkuCount(int skuCount) {
		this.skuCount = skuCount;
	}
	public String getArticle_type() {
		return article_type;
	}
	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}
	public String getBrand_type() {
		return brand_type;
	}
	public void setBrand_type(String brand_type) {
		this.brand_type = brand_type;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
}
