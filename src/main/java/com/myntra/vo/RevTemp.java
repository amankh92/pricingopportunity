package com.myntra.vo;

public class RevTemp {

	private double revenue = 0;
	private double td = 0;
	private double cd = 0;
	private double rgm = 0;
	private long qtySold = 0;
	
	private String commercial_type = null;
	private String article_type = null;
	private String brand_type = null;
	private String brand = null;
	private String gender = null;
	
	public double getRevenue() {
		return revenue;
	}
	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}
	public double getTd() {
		return td;
	}
	public void setTd(double td) {
		this.td = td;
	}
	public double getCd() {
		return cd;
	}
	public void setCd(double cd) {
		this.cd = cd;
	}
	public double getRgm() {
		return rgm;
	}
	public void setRgm(double rgm) {
		this.rgm = rgm;
	}
	public String getCommercial_type() {
		return commercial_type;
	}
	public void setCommercial_type(String commercial_type) {
		this.commercial_type = commercial_type;
	}
	public String getArticle_type() {
		return article_type;
	}
	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}
	public String getBrand_type() {
		return brand_type;
	}
	public void setBrand_type(String brand_type) {
		this.brand_type = brand_type;
	}
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getQtySold() {
		return qtySold;
	}
	public void setQtySold(long qtySold) {
		this.qtySold = qtySold;
	}
	
	
}
