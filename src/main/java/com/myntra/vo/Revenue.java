package com.myntra.vo;

public class Revenue {
	
	private double revTot = 0;
	private double tdTot = 0;
	private double cdTot = 0;
	private double rgmTot = 0;
	private int date = 0;
	private String commercial_type = null;
	private String article_type = null;
	private String brand_type = null;
	private String brand = null;
	private String gender = null;
	private double rev = 0;
	private double td = 0;
	private double cd = 0;
	private double rgm = 0;
	private double rev_cPerc = 0;
	private double td_cPerc = 0;
	private double cd_cPerc = 0;
	private double rgm_cPerc = 0;
	private long qtysold = 0;
	
	
	public long getQtysold() {
		return qtysold;
	}
	public void setQtysold(long qtysold) {
		this.qtysold = qtysold;
	}
	public double getRevTot() {
		return revTot;
	}
	public void setRevTot(double revTot) {
		this.revTot = revTot;
	}
	public double getTdTot() {
		return tdTot;
	}
	public void setTdTot(double tdTot) {
		this.tdTot = tdTot;
	}
	public double getCdTot() {
		return cdTot;
	}
	public void setCdTot(double cdTot) {
		this.cdTot = cdTot;
	}
	public double getRgmTot() {
		return rgmTot;
	}
	public void setRgmTot(double rgmTot) {
		this.rgmTot = rgmTot;
	}
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public String getCommercial_type() {
		return commercial_type;
	}
	public void setCommercial_type(String commercial_type) {
		this.commercial_type = commercial_type;
	}
	public String getArticle_type() {
		return article_type;
	}
	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}
	public String getBrand_type() {
		return brand_type;
	}
	public void setBrand_type(String brand_type) {
		this.brand_type = brand_type;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public double getRev() {
		return rev;
	}
	public void setRev(double rev) {
		this.rev = rev;
	}
	public double getTd() {
		return td;
	}
	public void setTd(double td) {
		this.td = td;
	}
	public double getCd() {
		return cd;
	}
	public void setCd(double cd) {
		this.cd = cd;
	}
	public double getRgm() {
		return rgm;
	}
	public void setRgm(double rgm) {
		this.rgm = rgm;
	}
	public double getRev_cPerc() {
		return rev_cPerc;
	}
	public void setRev_cPerc(double rev_cPerc) {
		this.rev_cPerc = rev_cPerc;
	}
	public double getTd_cPerc() {
		return td_cPerc;
	}
	public void setTd_cPerc(double td_cPerc) {
		this.td_cPerc = td_cPerc;
	}
	public double getCd_cPerc() {
		return cd_cPerc;
	}
	public void setCd_cPerc(double cd_cPerc) {
		this.cd_cPerc = cd_cPerc;
	}
	public double getRgm_cPerc() {
		return rgm_cPerc;
	}
	public void setRgm_cPerc(double rgm_cPerc) {
		this.rgm_cPerc = rgm_cPerc;
	}
	
}
