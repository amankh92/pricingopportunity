package com.myntra.vo;

import java.util.HashSet;
import java.util.Set;

public class CreateDropdownJSON {

	Set<String> commercial_types;
	Set<String> article_types;
	Set<String> brand_types;
	Set<String> brands;
	Set<String> genders;
	
	
	public Set<String> getCommercial_types() {
		return commercial_types;
	}
	public void setCommercial_types(Set<String> commercial_types) {
		this.commercial_types = commercial_types;
	}
	public Set<String> getArticle_types() {
		return article_types;
	}
	public void setArticle_types(Set<String> article_types) {
		this.article_types = article_types;
	}
	public Set<String> getBrand_types() {
		return brand_types;
	}
	public void setBrand_types(Set<String> brand_types) {
		this.brand_types = brand_types;
	}
	public Set<String> getBrands() {
		return brands;
	}
	public void setBrands(Set<String> brands) {
		this.brands = brands;
	}
	public Set<String> getGenders() {
		return genders;
	}
	public void setGenders(Set<String> genders) {
		this.genders = genders;
	}
	
}
