package com.myntra.vo;

public class FinalVO {
	
	private String style_id = null;
	private String commercial_type = null;
	private String article_type = null;
	private String brand_type = null;
	private String brand = null;
	private String gender = null;
	double beforeConversion;
	double afterConversion;
	double beforePdp = 0;
	double afterPdp = 0;
	double beforeQtySold = 0;
	double afterQtySold = 0;
	
	public String getStyle_id() {
		return style_id;
	}
	public void setStyle_id(String style_id) {
		this.style_id = style_id;
	}
	public String getCommercial_type() {
		return commercial_type;
	}
	public void setCommercial_type(String commercial_type) {
		this.commercial_type = commercial_type;
	}
	public String getArticle_type() {
		return article_type;
	}
	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}
	public String getBrand_type() {
		return brand_type;
	}
	public void setBrand_type(String brand_type) {
		this.brand_type = brand_type;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public double getBeforeConversion() {
		return beforeConversion;
	}
	public void setBeforeConversion(double beforeConversion) {
		this.beforeConversion = beforeConversion;
	}
	public double getAfterConversion() {
		return afterConversion;
	}
	public void setAfterConversion(double afterConversion) {
		this.afterConversion = afterConversion;
	}
	public double getBeforePdp() {
		return beforePdp;
	}
	public void setBeforePdp(double beforePdp) {
		this.beforePdp = beforePdp;
	}
	public double getAfterPdp() {
		return afterPdp;
	}
	public void setAfterPdp(double afterPdp) {
		this.afterPdp = afterPdp;
	}
	public double getBeforeQtySold() {
		return beforeQtySold;
	}
	public void setBeforeQtySold(double beforeQtySold) {
		this.beforeQtySold = beforeQtySold;
	}
	public double getAfterQtySold() {
		return afterQtySold;
	}
	public void setAfterQtySold(double afterQtySold) {
		this.afterQtySold = afterQtySold;
	}
	
}
