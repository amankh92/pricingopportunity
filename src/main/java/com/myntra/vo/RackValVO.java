package com.myntra.vo;

public class RackValVO {

	private String article_type = null;
	private String brand_type = null;
	private String brand = null;
	private String gender = null;
	private String rack = null;
	private String collection = null;
	private int styleCount;
	private long pdpAbsolute;
	private double pdpPerDay;
	private double qtySoldNormalised;
	
	public String getArticle_type() {
		return article_type;
	}
	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}
	public String getBrand_type() {
		return brand_type;
	}
	public void setBrand_type(String brand_type) {
		this.brand_type = brand_type;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getRack() {
		return rack;
	}
	public void setRack(String rack) {
		this.rack = rack;
	}
	public String getCollection() {
		return collection;
	}
	public void setCollection(String collection) {
		this.collection = collection;
	}
	public int getStyleCount() {
		return styleCount;
	}
	public void setStyleCount(int styleCount) {
		this.styleCount = styleCount;
	}
	public long getPdpAbsolute() {
		return pdpAbsolute;
	}
	public void setPdpAbsolute(long pdpAbsolute) {
		this.pdpAbsolute = pdpAbsolute;
	}
	public double getPdpPerDay() {
		return pdpPerDay;
	}
	public void setPdpPerDay(double pdpPerDay) {
		this.pdpPerDay = pdpPerDay;
	}
	public double getQtySoldNormalised() {
		return qtySoldNormalised;
	}
	public void setQtySoldNormalised(double qtySoldNormalised) {
		this.qtySoldNormalised = qtySoldNormalised;
	}
	
}
