package com.myntra.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.myntra.util.Utility;
import com.myntra.vo.ElasticityRackVo;

public class Test {

	public static void main(String[] args) {
		run();
	}

	public static void run() {

		//String listNameStr = "sorted pdpIncQtyIncList";
		String listNameStr = "sorted pdpDecQtyIncList";
		//String listNameStr = "sorted pdpIncQtyDecList";
		//String listNameStr = "sorted pdpDecQtyDecList";
		
		List<String> selectedStyles = new ArrayList<String>();

		//File file_1 = new File("/home/en-sorabhk/Documents/newFiles/brokenAnalysis_lastWeek");
		//File file_1 = new File("/home/en-sorabhk/Documents/newFiles/analysis");
		File file_1 = new File("/home/en-sorabhk/workspace/BrokenNonBrokenConversionTracking/console.txt");
		
		BufferedReader in_1 = null;
		try {
			in_1 = new BufferedReader(new FileReader(file_1));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String str_1;
		try {
			String[] strArr = null;
			while ((str_1 = in_1.readLine()) != null) {
				if(str_1.startsWith(listNameStr)) {   
					String listStr = str_1.substring(str_1.indexOf('[')+1, str_1.indexOf(']'));
					strArr = listStr.split(",");
					for (String style : strArr) {
						selectedStyles.add(style.trim());
					}
				}
			}
			in_1.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(selectedStyles.size());

		
		//current visibility ranking
		
		List<String> visibilityList = new ArrayList<String>();

		File file_2 = new File("/home/en-sorabhk/Documents/styles");

		BufferedReader in_2 = null;
		try {
			in_2 = new BufferedReader(new FileReader(file_2));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String str_2;
		try {
			while ((str_2 = in_2.readLine()) != null) {
				visibilityList.add(str_2.trim());
			}
			in_2.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		System.out.println(visibilityList.size());

		
		List<String> currentVisibilitySelectedList = new ArrayList<String>();
		
		for (String style : visibilityList) {
			if (selectedStyles.contains(style)) {
				currentVisibilitySelectedList.add(style);
			}
		}
		
		System.out.println(currentVisibilitySelectedList.size());
		
		
		//removing styles that are not in current list
		List<String> finalSelectedList = new ArrayList<String>();
		for (String style : selectedStyles) {
			if (currentVisibilitySelectedList.contains(style)) {
				finalSelectedList.add(style);
			}
		}
		
		System.out.println(finalSelectedList.size());
		
		int moveUp = 0;
		int moveDown = 0;
		
		for (String style : finalSelectedList) {
			int selectedRank = finalSelectedList.indexOf(style);
			int currentRank = currentVisibilitySelectedList.indexOf(style);
			
			if (currentRank > selectedRank) {
				moveUp++;
			} else if (currentRank < selectedRank){
				moveDown++;
			}
		}
		
		System.out.println(moveUp + "        " + moveDown);
		
		
		/*int visibilityChangeCount = 0;

		for (int i=0; i < selectedStyles.size()/2; i++) {
			String style1 = selectedStyles.get(i);
			String style2 = selectedStyles.get(selectedStyles.size() - i -1);

			int style1_index = visibilityList.indexOf(style1);
			int style2_index = visibilityList.indexOf(style2);

			if (style1_index > style2_index) {
				visibilityChangeCount++;
			}

		}

		System.out.println("changes in visibility  :  "+visibilityChangeCount);*/

	}
}
